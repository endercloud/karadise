package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.commands.RulesCommand;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.util.Scheduler;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.title.Title;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.time.Duration;

@Singleton
@AutoService(Service.class)
public class PlayerConnectionService implements Service {

    private final LuckPerms luckPerms;
    private final PlayerRepository playerRepository;
    private final KaradiserRepository karadiserRepository;
    private final ClaimRepository claimRepository;


    @Inject
    public PlayerConnectionService(LuckPerms luckPerms, PlayerRepository playerRepository, KaradiserRepository karadiserRepository, ClaimRepository claimRepository) {
        this.luckPerms = luckPerms;
        this.playerRepository = playerRepository;
        this.karadiserRepository = karadiserRepository;
        this.claimRepository = claimRepository;
    }

    @Override
    public void init() {

    }

    public void handlePreLogin(PlayerLoginEvent event) {
        if (event.getResult() == PlayerLoginEvent.Result.KICK_FULL) {
            if (event.getPlayer().hasPermission("karadise.joinlimit.bypass")) {
                event.allow();
            }
            if (Bukkit.getOnlinePlayers().stream().filter(player -> !player.hasPermission("karadise.joinlimit.ignore")).count() < Bukkit.getMaxPlayers()) {
                event.allow();
            }
        }
    }

    public void handlePlayerLogin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        String prefix = "";
        User user = this.luckPerms.getUserManager().getUser(player.getUniqueId());
        Group group = null;
        if (user != null && (group = this.luckPerms.getGroupManager().getGroup(user.getPrimaryGroup())) != null && group.getCachedData().getMetaData().getPrefix() != null) {
            prefix = ChatColor.translateAlternateColorCodes('&', group.getCachedData().getMetaData().getPrefix());
        }

        if (!prefix.isEmpty()) {
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
            Team team = scoreboard.getTeam(group.getName().toLowerCase());
            if (team != null) {
                team.addEntry(event.getPlayer().getName());
            }
        }

        if (!player.hasPlayedBefore()) {
            event.setJoinMessage(ChatColor.GRAY + "[" + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "*" + ChatColor.GRAY + "] Welcome " + event.getPlayer().getDisplayName() + " to KaradiseSMP");
            Toolbox.getAudiences().player(player).openBook(RulesCommand.RULES_BOOK);
        } else {
            event.setJoinMessage(ChatColor.GRAY + "[" + ChatColor.GREEN + "+" + ChatColor.GRAY + "] " + (prefix.isEmpty() ? "" : prefix + " ") + event.getPlayer().getDisplayName() + ChatColor.GRAY + " joined the server");
        }

        Scheduler.async(() -> {

            PlayerData playerData;
            if ((playerData = this.playerRepository.getPlayerData(player.getUniqueId())) == null) {
                playerData = this.playerRepository.createPlayerData(player);
            } else {
                this.playerRepository.updateNameSkinLast(playerData, player.getName(), this.playerRepository.getPlayerSkinURL(player));
            }

            this.playerRepository.cachePlayerData(playerData);

            Karadiser karadiser;
            if ((karadiser = this.karadiserRepository.getKaradiserData(playerData.getId())) == null) {
                this.karadiserRepository.createKaradiserData(playerData.getId());
            } else {
                this.karadiserRepository.cacheKaradiser(karadiser);


                //Update Island Owner Cached Name for islands
                for (Object idObj : karadiser.getOwnedClaims().keySet()) {
                    long id = (long) idObj;
                    Claim c = this.claimRepository.getCachedClaim(id);

                    if (!c.getCachedOwnerName().equals(player.getName())) {
                        this.claimRepository.updateOwnerCachedName(id, player.getName());
                    }
                }
            }
        });

        Scheduler.laterAsync(() -> this.showTicker(player), 40L);
    }


    public void handleLogout(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        String prefix = "";
        User user = this.luckPerms.getUserManager().getUser(player.getUniqueId());
        Group group = null;
        if (user != null && (group = this.luckPerms.getGroupManager().getGroup(user.getPrimaryGroup())) != null && group.getCachedData().getMetaData().getPrefix() != null) {
            prefix = ChatColor.translateAlternateColorCodes('&', group.getCachedData().getMetaData().getPrefix());
        }

        event.setQuitMessage(ChatColor.GRAY + "[" + ChatColor.RED + "-" + ChatColor.GRAY + "] " + ChatColor.GRAY + (prefix.isEmpty() ? "" : prefix + " ") + player.getDisplayName() + ChatColor.GRAY + " left the server");

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        Team team = scoreboard.getEntryTeam(player.getName());
        if (team != null) {
            team.removeEntry(player.getName());
        }

        PlayerData data = this.playerRepository.getCachedPlayerData(player.getUniqueId());
        if (data != null) {
            this.karadiserRepository.unloadKaradiserData(player.getUniqueId());
        }
        this.playerRepository.unloadPlayerData(player.getUniqueId());
    }


    private void showTicker(Player player) {

        String intro = "Karadise";
        Audience audience = Toolbox.getAudiences().player(player);

        int delay = 0;
        for (int i = 0; i <= intro.length(); i++) {

            String displayText;
            if (i == intro.length()) {
                displayText = intro;
            } else {
                displayText = i == 0 ? "" : intro.substring(0, i);
            }
            if (i % 2 == 0) {
                displayText += "|";
            }
            String finalDisplayText = displayText;
            Scheduler.laterAsync(() -> {
                audience.showTitle(Title.title(
                        Component.text(finalDisplayText, TextColor.color(253, 189, 255), TextDecoration.BOLD),
                        Component.text(""),
                        Title.Times.of(Duration.ofMillis(0),
                                Duration.ofSeconds(5),
                                Duration.ofMillis(0))
                ));
            }, delay);

            if (i == intro.length()) {
                delay += 5;
            } else {
                delay += 2;
            }
        }
        Scheduler.laterAsync(() -> {
            audience.showTitle(Title.title(
                    Component.text("Karadise", TextColor.color(253, 189, 255), TextDecoration.BOLD),
                    Component.text("Welcome!", TextColor.color(167, 139, 201)),
                    Title.Times.of(Duration.ofMillis(0),
                            Duration.ofSeconds(5),
                            Duration.ofMillis(0))
            ));
        }, delay);

    }

}
