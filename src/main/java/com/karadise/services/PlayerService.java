package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.HomeWarp;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.repos.KaradiserRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.database.AsyncDatabase;
import com.rhonim.toolbox.entities.position.WorldPosition;
import com.rhonim.toolbox.injection.Service;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Singleton
@AutoService(Service.class)
public class PlayerService implements Service {

    private final KaradiserRepository karadiserRepository;

    @Inject
    public PlayerService(KaradiserRepository karadiserRepository) {
        this.karadiserRepository = karadiserRepository;
    }

    @Override
    public void init() {

    }

    public void handleHomeCommand(CommandSender commandSender) {
        if (!(commandSender instanceof Player)) {
            return;
        }

        Player player = (Player) commandSender;

        Audience audience = Toolbox.getAudiences().player(player);

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
        HomeWarp home = karadiser.getHome();
        if (home == null) {
            audience.sendMessage(Identity.nil(), Component.text("You don't have a home set!", NamedTextColor.RED));
            return;
        }

        player.teleport(home.getPosition().toLocation());
        audience.sendMessage(Identity.nil(), Component.text("Woosh!", NamedTextColor.GREEN));
    }

    public void handleSetHomeCommand(CommandSender sender) {
        Player player = (Player) sender;

        Audience audience = Toolbox.getAudiences().player(player);
        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
        if (karadiser.getHome() != null) {
            HomeWarp warp = karadiser.getHome();
            warp.setPosition(new WorldPosition(player.getLocation()));

            AsyncDatabase.submit(warp::save);
        } else {
            AsyncDatabase.submit(() -> this.karadiserRepository.createHome(karadiser.getId(), new WorldPosition(player.getLocation())));
        }

        audience.sendMessage(Identity.nil(), Component.text("Your home has been set.", NamedTextColor.GREEN));
    }

    public void handleShoutToggle(Player player) {
        Audience audience = Toolbox.getAudiences().player(player);

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
        karadiser.setShouting(!karadiser.isShouting());


        if (karadiser.isShouting()) {
            audience.sendMessage(Identity.nil(), Component.text("You are now shouting!", NamedTextColor.GREEN));
        } else {
            audience.sendMessage(Identity.nil(), Component.text("You are no longer shouting!", NamedTextColor.GREEN));
        }
    }
}
