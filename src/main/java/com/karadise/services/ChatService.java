package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.repos.KaradiserRepository;
import com.rhonim.toolbox.injection.Service;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.Iterator;

@Singleton
@AutoService(Service.class)
public class ChatService implements Service {
    private final LuckPerms luckPerms;
    private final KaradiserRepository karadiserRepository;

    @Inject
    public ChatService(LuckPerms luckPerms, KaradiserRepository karadiserRepository) {
        this.luckPerms = luckPerms;
        this.karadiserRepository = karadiserRepository;
    }

    @Override
    public void init() {

    }

    public void handleChatEvent(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Player player = event.getPlayer();

        String prefix = "";
        String suffix = "";
        User user = this.luckPerms.getUserManager().getUser(player.getUniqueId());
        Group group;
        if (user != null && (group = this.luckPerms.getGroupManager().getGroup(user.getPrimaryGroup())) != null) {
            if (group.getCachedData().getMetaData().getPrefix() != null) {
                prefix = ChatColor.translateAlternateColorCodes('&', group.getCachedData().getMetaData().getPrefix());
            }
            if (group.getCachedData().getMetaData().getSuffix() != null) {
                suffix = ChatColor.translateAlternateColorCodes('&', group.getCachedData().getMetaData().getSuffix());
            }
        }

        String message = event.getMessage();

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
        boolean global = karadiser.isShouting();
        if (!global && message.startsWith("!") && !message.startsWith("!!")) {
            event.setMessage(message.substring(1));
            global = true;
        }

        event.setFormat((global ? "" + ChatColor.RED + ChatColor.BOLD + "S" + ChatColor.WHITE + " " : "") + (prefix.isEmpty() ? "" : prefix + " ") + "%s" + (suffix.isEmpty() ? "" : " " + suffix) + ChatColor.GRAY + " > " + ChatColor.WHITE + "%s");


        Iterator<Player> it = event.getRecipients().iterator();
        while (it.hasNext()) {
            Player pl = it.next();
            if (!global && (pl.getWorld() != player.getWorld() || pl.getLocation().distanceSquared(player.getLocation()) >= 100 * 100)) {
                it.remove();
            }
        }
    }
}
