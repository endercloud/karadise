package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.ClaimMember;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.enums.ClaimType;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.database.AsyncDatabase;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.util.EntityUtil;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Dispenser;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.Cancellable;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Singleton
@AutoService(Service.class)
public class ClaimService implements Service {
    public static final List<Material> FARM_BLOCKS = Arrays.asList(
            Material.WHEAT, Material.MELON, Material.PUMPKIN, Material.CARROT, Material.PUMPKIN_STEM,
            Material.MELON_STEM, Material.POTATO, Material.BEETROOT, Material.POTATO, Material.CHORUS_FRUIT,
            Material.CHORUS_FLOWER, Material.CHORUS_PLANT, Material.CARROTS
    );

    public static final List<Material> BLOCKED_BLOCKS = Arrays.asList(
            Material.OAK_FENCE_GATE, Material.ACACIA_FENCE_GATE, Material.BIRCH_FENCE_GATE, Material.DARK_OAK_FENCE_GATE,
            Material.JUNGLE_FENCE_GATE, Material.SPRUCE_FENCE_GATE, Material.ACACIA_DOOR, Material.BIRCH_DOOR,
            Material.DARK_OAK_DOOR, Material.JUNGLE_DOOR, Material.SPRUCE_DOOR, Material.ACACIA_TRAPDOOR, Material.BIRCH_TRAPDOOR,
            Material.CRIMSON_TRAPDOOR, Material.DARK_OAK_TRAPDOOR, Material.JUNGLE_TRAPDOOR, Material.OAK_TRAPDOOR,
            Material.SPRUCE_TRAPDOOR, Material.WARPED_TRAPDOOR, Material.ACACIA_DOOR, Material.BIRCH_DOOR, Material.CRIMSON_DOOR,
            Material.DARK_OAK_DOOR, Material.IRON_TRAPDOOR, Material.JUNGLE_DOOR, Material.OAK_DOOR, Material.SPRUCE_DOOR, Material.WARPED_DOOR,
            Material.CHEST, Material.STONE_BUTTON, Material.ACACIA_BUTTON, Material.BIRCH_BUTTON, Material.CRIMSON_BUTTON,
            Material.DARK_OAK_BUTTON, Material.JUNGLE_BUTTON, Material.OAK_BUTTON, Material.POLISHED_BLACKSTONE_BUTTON,
            Material.SPRUCE_BUTTON, Material.WARPED_BUTTON, Material.LEVER, Material.ITEM_FRAME, Material.CAKE, Material.BLACK_BED,
            Material.BLUE_BED, Material.BROWN_BED, Material.CYAN_BED, Material.GRAY_BED, Material.GREEN_BED, Material.LIGHT_BLUE_BED,
            Material.LIGHT_GRAY_BED, Material.LIME_BED, Material.MAGENTA_BED, Material.ORANGE_BED, Material.PINK_BED, Material.PURPLE_BED,
            Material.RED_BED, Material.WHITE_BED, Material.YELLOW_BED, Material.TRIPWIRE, Material.TRIPWIRE_HOOK,
            Material.TRAPPED_CHEST, Material.IRON_DOOR, Material.IRON_TRAPDOOR, Material.ACACIA_PRESSURE_PLATE, Material.POLISHED_BLACKSTONE_PRESSURE_PLATE,
            Material.BIRCH_PRESSURE_PLATE, Material.CRIMSON_PRESSURE_PLATE, Material.DARK_OAK_PRESSURE_PLATE, Material.HEAVY_WEIGHTED_PRESSURE_PLATE,
            Material.JUNGLE_PRESSURE_PLATE, Material.LIGHT_WEIGHTED_PRESSURE_PLATE, Material.OAK_PRESSURE_PLATE, Material.SPRUCE_PRESSURE_PLATE,
            Material.STONE_PRESSURE_PLATE, Material.WARPED_PRESSURE_PLATE, Material.FURNACE, Material.HOPPER, Material.DROPPER,
            Material.DISPENSER, Material.JUKEBOX, Material.NOTE_BLOCK, Material.REPEATER,
            Material.COMPARATOR
    );

    public static final List<Material> BLOCKED_HAND = Arrays.asList(
            Material.FLINT_AND_STEEL, Material.ITEM_FRAME, Material.ARMOR_STAND, Material.MINECART,
            Material.TNT_MINECART, Material.HOPPER_MINECART, Material.CHEST_MINECART, Material.FURNACE_MINECART,
            Material.SMOKER, Material.BREWING_STAND, Material.LECTERN, Material.BARREL, Material.SHULKER_BOX, Material.BLAST_FURNACE,
            Material.DIAMOND_HOE, Material.GOLDEN_HOE, Material.IRON_HOE, Material.STONE_HOE, Material.WOODEN_HOE,
            Material.INK_SAC
    );

    public static final List<EntityType> BLOCKED_ENTITIES = Arrays.asList(
            EntityType.ARMOR_STAND, EntityType.ITEM_FRAME, EntityType.MINECART, EntityType.MINECART_CHEST,
            EntityType.MINECART_FURNACE, EntityType.MINECART_HOPPER, EntityType.VILLAGER, EntityType.HORSE,
            EntityType.BOAT
    );

    private final ClaimRepository claimRepository;
    private final KaradiserRepository karadiserRepository;

    @Inject
    public ClaimService(ClaimRepository claimRepository, KaradiserRepository karadiserRepository) {
        this.claimRepository = claimRepository;
        this.karadiserRepository = karadiserRepository;
    }

    @Override
    public void init() {
        this.claimRepository.init();
    }


    public void doClaim(Player player, ClaimType type) {
        Audience audience = Toolbox.getAudiences().player(player);


        if (this.claimRepository.isClaimed(player.getLocation())) {//Check if its claimed this way so we don't double add an owner...
            Claim currentClaim = this.claimRepository.getCachedClaim(player.getLocation());
            audience.sendMessage(Identity.nil(), Component.text("This chunk is already claimed by " + currentClaim.getCachedOwnerName() + "!", NamedTextColor.RED));
            return;
        }

        if (player.getLocation().getWorld().getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName() + "_the_end")) {
            audience.sendMessage(Identity.nil(), Component.text("Claims cannot be made in the end!", NamedTextColor.RED));
            return;
        }

        ItemStack hand = player.getInventory().getItemInMainHand();
        if (!hand.getType().equals(Material.IRON_BLOCK)) {
            audience.sendMessage(Identity.nil(), Component.text("You must be holding an Iron Block in your hand to claim a chunk!", NamedTextColor.RED));
            return;
        }

        hand.subtract();

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());

        this.claimRepository.addCachedClaimOwner(player.getLocation(), karadiser.getId());
        AsyncDatabase.submit(() -> {
            Location loc = player.getLocation();
            Claim claim = this.claimRepository.createClaim(karadiser.getId(), player.getName(), type, loc.getWorld().getName(), loc.getBlockX() >> 4, loc.getBlockZ() >> 4);

            for (long targetId : this.claimRepository.getGlobalMemberships(karadiser.getId()).keySet()) {
                this.claimRepository.createClaimMembership(claim.getId(), targetId);
            }
        });
    }

    public void addGlobalMember(Karadiser owner, Karadiser target, PlayerData targetPlayerData) {
        this.claimRepository.cacheGlobalMembership(owner.getId(), target.getId(), targetPlayerData);

        AsyncDatabase.submit(() -> {
            this.claimRepository.createGlobalMembership(owner.getId(), target.getId());

            for (Claim c : owner.getOwnedClaims().values()) {
                Claim claim = this.claimRepository.getCachedClaim(c.getId());

                if (claim.isMember(target.getId())) {
                    continue;
                }
                this.claimRepository.createClaimMembership(claim.getId(), target.getId());
            }
        });
    }

    public void handleBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Claim claim = this.claimRepository.getCachedClaim(block.getLocation());

        if (!hasAccess(event.getPlayer(), claim)) {
            if (claim != null && claim.getClaimType().equals(ClaimType.FARM) && FARM_BLOCKS.contains(block.getType())) {
                return;
            }

            event.setCancelled(true);
        }
    }

    public void handleBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        Claim claim = this.claimRepository.getCachedClaim(block.getLocation());

        if (!hasAccess(event.getPlayer(), claim)) {
            if (claim != null && claim.getClaimType().equals(ClaimType.FARM) && FARM_BLOCKS.contains(block.getType())) {
                return;
            }

            event.setCancelled(true);
        }
    }

    public void handleWaterFlow(BlockFromToEvent event) {
        //Always allow flowing straight down
        if (event.getFace().equals(BlockFace.DOWN)) {
            return;
        }

        //Don't let water to go from one camp to another

        Claim toClaim = this.claimRepository.getCachedClaim(event.getToBlock().getLocation());
        if (toClaim == null) {
            return;
        }

        Claim fromClaim = this.claimRepository.getCachedClaim(event.getBlock().getLocation());
        if (fromClaim == null || toClaim.getOwnerId() != fromClaim.getOwnerId()) {
            event.setCancelled(true);
        }
    }

    public void handleExplode(EntityExplodeEvent event) {
        Claim claim = this.claimRepository.getCachedClaim(event.getEntity().getLocation());
        if (claim != null) {
            event.setCancelled(true);
            return;
        }

        List<Block> blocks = event.blockList();
        for (int i = 0; i < blocks.size(); i++) {
            Block block = event.blockList().get(i);

            if (this.claimRepository.getCachedClaim(block.getLocation()) != null) {
                blocks.remove(block);
            }
        }
    }

    public void handlePistonPull(BlockPistonRetractEvent event) {
        Location retractLoc;
        if (!event.isSticky()
                || event.getDirection().equals(BlockFace.DOWN)
                || event.getDirection().equals(BlockFace.UP)
                || (retractLoc = event.getBlock().getRelative(event.getDirection(), 2).getLocation()).getBlock().getType().equals(Material.AIR)) {
            return;
        }

        long movingBlockOwner = -1;
        Claim movingBlockClaim = this.claimRepository.getCachedClaim(retractLoc);
        if (movingBlockClaim != null) {
            movingBlockOwner = movingBlockClaim.getOwnerId();
        }

        long pistonOwner = -2;
        Claim pistonClaim = this.claimRepository.getCachedClaim(event.getBlock().getLocation());
        if (pistonClaim != null) {
            pistonOwner = pistonClaim.getOwnerId();
        }

        if (pistonOwner != movingBlockOwner) {
            event.setCancelled(true);
        }
    }

    public void handlePistonPush(BlockPistonExtendEvent event) {
        Block piston = event.getBlock();
        Claim pistonClaim = this.claimRepository.getCachedClaim(piston.getLocation());
        if (pistonClaim == null) {
            event.setCancelled(true);
            return;
        }

        if (event.getDirection().equals(BlockFace.DOWN) || event.getDirection() == BlockFace.UP) {
            return;
        }

        List<Block> blocks = event.getBlocks();
        if (blocks.size() == 0) {
            Block invadedBlock = piston.getRelative(event.getDirection());
            if (invadedBlock.getType().equals(Material.AIR)) {
                return;
            }

            if (this.claimRepository.getCachedClaim(invadedBlock.getLocation()) != null) {
                event.setCancelled(true);
            }

            return;
        }

        for (Block block : blocks) {
            Claim currBlockClaim = this.claimRepository.getCachedClaim(block.getLocation());
            if (currBlockClaim != null && currBlockClaim.getOwnerId() != pistonClaim.getOwnerId()) {
                event.setCancelled(true);
                event.getBlock().getWorld().createExplosion(event.getBlock().getLocation(), 0.0F);
                event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), new ItemStack(event.getBlock().getType()));
                event.getBlock().setType(Material.AIR);
                return;
            }

            Claim newBlockClaim = this.claimRepository.getCachedClaim(block.getRelative(event.getDirection()).getLocation());
            if (newBlockClaim == null || newBlockClaim.getOwnerId() != pistonClaim.getOwnerId()) {
                event.setCancelled(true);
                event.getBlock().getWorld().createExplosion(event.getBlock().getLocation(), 0.0F);
                event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), new ItemStack(event.getBlock().getType()));
                event.getBlock().setType(Material.AIR);
                return;
            }
        }
    }

    public void handleGrow(StructureGrowEvent event) {
        Claim rootClaim = this.claimRepository.getCachedClaim(event.getLocation());
        for (BlockState b : new ArrayList<>(event.getBlocks())) {
            Claim blockClaim = this.claimRepository.getCachedClaim(b.getLocation());
            if (blockClaim != null) {
                if (rootClaim == null || rootClaim.getOwnerId() != blockClaim.getOwnerId()) {
                    event.getBlocks().remove(b);
                }
            }
        }
    }

    public void handleHangingBreak(HangingBreakByEntityEvent event) {
        Player remover = EntityUtil.getPlayerFromEntity(event.getRemover());
        if (remover == null) {
            return;
        }
        if (!hasAccess(remover, event.getEntity().getLocation())) {
            event.setCancelled(true);
        }
    }

    public void handleDispense(BlockDispenseEvent event) {
        Block fromBlock = event.getBlock();
        BlockData fromData = fromBlock.getBlockData();
        if (!(fromData instanceof Dispenser)) {
            return;
        }

        Dispenser dispenser = (Dispenser) fromData;
        Block toBlock = fromBlock.getRelative(dispenser.getFacing());

        Claim placedClaim = this.claimRepository.getCachedClaim(fromBlock.getLocation());
        Claim toClaim = this.claimRepository.getCachedClaim(toBlock.getLocation());

        Material typeDispensed = event.getItem().getType();

        if (placedClaim != null) {
            if ((toClaim == null || placedClaim.getOwnerId() != toClaim.getOwnerId()) && (typeDispensed.equals(Material.WATER_BUCKET) || typeDispensed.equals(Material.LAVA_BUCKET))) {
                event.setCancelled(true);
            }
        }

    }

    public void handleIgnite(BlockIgniteEvent event) {
        if (!event.getCause().equals(BlockIgniteEvent.IgniteCause.SPREAD)) {
            return;
        }

        if (event.getIgnitingEntity() != null) {
            Player player = EntityUtil.getPlayerFromEntity(event.getIgnitingEntity());
            if (player == null) {
                return;
            }

            if (!hasAccess(player, event.getBlock().getLocation())) {
                event.setCancelled(true);
                return;
            }
        }

        if (event.getIgnitingBlock() != null) {
            Claim fromClaim = this.claimRepository.getCachedClaim(event.getIgnitingBlock().getLocation());
            Claim toClaim = this.claimRepository.getCachedClaim(event.getBlock().getLocation());
            if (toClaim != null) {
                if (fromClaim == null || toClaim.getOwnerId() != fromClaim.getOwnerId()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    public void handleBucketFill(PlayerBucketFillEvent event) {
        Player player = event.getPlayer();
        if (!hasAccess(player, event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    public void handleBucketEmpty(PlayerBucketEmptyEvent event) {
        Player player = event.getPlayer();
        if (!hasAccess(player, event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    public void handleHangingPlace(HangingPlaceEvent event) {
        Player player = event.getPlayer();
        if (player == null) {
            return;
        }

        if (!this.hasAccess(player, event.getEntity().getLocation())) {
            event.setCancelled(true);
        }
    }

    public void handleBlockInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            return;
        }

        Block block = event.getClickedBlock();
        ItemStack hand = event.getItem();

        Claim claim = this.claimRepository.getCachedClaim(block.getLocation());
        if (claim != null &&
                (BLOCKED_BLOCKS.contains(block.getType()) || (hand != null && BLOCKED_HAND.contains(hand.getType())) || event.getAction().equals(Action.PHYSICAL))) {
            if (!this.hasAccess(event.getPlayer(), claim)) {
                event.setCancelled(true);
            }
        }
    }

    public void handleEntityInteract(Player player, Entity entity, Cancellable event) {
        Claim claim = this.claimRepository.getCachedClaim(entity.getLocation());
        if (claim != null && BLOCKED_ENTITIES.contains(entity.getType()) && !this.hasAccess(player, claim)) {
            event.setCancelled(true);
        }
    }

    public void handleTame(EntityTameEvent event) {


    }

    public void handleVehicle(Entity entity, Vehicle vehicle, Cancellable event) {
        if (!(entity instanceof Player)) {
            return;
        }

        Claim claim = this.claimRepository.getCachedClaim(vehicle.getLocation());
        if (claim != null && !this.hasAccess((Player) entity, claim)) {
            event.setCancelled(true);
        }

    }

    public boolean hasAccess(Player player, String worldName, long chunkX, long chunkZ) {
        Claim claim = this.claimRepository.getCachedClaim(worldName, chunkX, chunkZ);
        return this.hasAccess(player, claim);
    }

    public boolean hasAccess(Player player, Location location) {
        Claim claim = this.claimRepository.getCachedClaim(location);
        return this.hasAccess(player, claim);
    }

    public boolean hasAccess(Player player, Claim claim) {
        if (claim == null) {
            return true;
        }

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());

        return claim.getOwnerId() == karadiser.getId() || this.isMember(claim, karadiser);

    }

    public boolean isMember(Claim claim, Karadiser karadiser) {
       /* ImmutableMap<Long, PlayerData> global = this.claimRepository.getGlobalMemberships(claim.getOwnerId());
        if (global != null && global.containsKey(karadiser.getId())) {
            return true;
        }*/

        for (ClaimMember cm : claim.getClaimMembers()) {
            if (cm.getKaradiserId() == karadiser.getId()) {
                return true;
            }
        }
        return  claim.getOwnerId() == karadiser.getId();
    }
}
