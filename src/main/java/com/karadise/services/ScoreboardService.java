package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.injection.Service;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;

@Singleton
@AutoService(Service.class)
public class ScoreboardService implements Service {

    private final LuckPerms luckPerms;

    @Inject
    public ScoreboardService(LuckPerms luckPerms) {
        this.luckPerms = luckPerms;
    }

    @Override
    public void init() {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        for (Team team : new ArrayList<>(scoreboard.getTeams())) {
            team.unregister();
        }

        for (Group g : this.luckPerms.getGroupManager().getLoadedGroups()) {
            Team team = scoreboard.registerNewTeam(g.getName());
            team.setDisplayName(g.getName());
            String prefix = g.getCachedData().getMetaData().getPrefix();
            team.setPrefix(prefix != null ? (ChatColor.stripColor(prefix).trim().length() > 0 ? prefix + " " : prefix) : "");
            String suffix = g.getCachedData().getMetaData().getSuffix();
            team.setSuffix(suffix != null ? (ChatColor.stripColor(suffix).trim().length() > 0 ? " " + suffix : suffix) : "");
            team.setCanSeeFriendlyInvisibles(false);
        }
    }
}
