package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.enums.ClaimType;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.CombatRepository;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.util.Scheduler;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.projectiles.ProjectileSource;

@Singleton
@AutoService(Service.class)
public class CombatService implements Service {

    private final ClaimService claimService;

    private final CombatRepository combatRepository;
    private final ClaimRepository claimRepository;

    @Inject
    public CombatService(ClaimService claimService, CombatRepository combatRepository, ClaimRepository claimRepository) {
        this.claimService = claimService;
        this.combatRepository = combatRepository;
        this.claimRepository = claimRepository;
    }

    @Override
    public void init() {

    }


    public void handleDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        if (this.combatRepository.isImmune(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    public void handleDamageEntity(EntityDamageByEntityEvent event) {
        Entity damager = event.getDamager();
        Entity entity = event.getEntity();

        if (damager instanceof Projectile) {
            ProjectileSource source = ((Projectile) damager).getShooter();
            if (source instanceof Entity) {
                damager = (Entity) source;
            } else {
                return;
            }
        }

        Claim claim = this.claimRepository.getCachedClaim(entity.getLocation());

        if (entity instanceof Player) {
            if (claim != null) {
                if (damager instanceof Monster) {
                    event.setCancelled(true);
                } else if (damager instanceof Creature) {
                    event.setCancelled(true);
                } else if (damager instanceof Player) {
                    Player d = (Player) damager;
                    if (!this.claimService.hasAccess(d, claim)) {
                        event.setCancelled(true);
                    }
                }
            }
        }

        if (damager instanceof Player) {
            Player d = (Player) damager;
            if (claim != null && !this.claimService.hasAccess(d, claim)) {
                if (claim.getClaimType().equals(ClaimType.SPAWNER) && entity instanceof Monster) {
                    return;
                }

                if (claim.getClaimType().equals(ClaimType.FARM) &&
                        (entity instanceof Sheep
                                || entity instanceof Pig
                                || entity instanceof Cow
                                || entity instanceof Chicken
                                || entity instanceof Squid
                                || entity instanceof Horse
                                || entity instanceof Rabbit
                                || entity instanceof Llama
                        )) {
                    return;
                }

                event.setCancelled(true);
            }
        }

        if (entity instanceof Minecart && claim != null) {
            if (!(damager instanceof Player) || !this.claimService.hasAccess((Player) damager, claim)) {
                event.setCancelled(true);
            }
        }
    }

    public void handleRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        this.combatRepository.setImmune(player.getUniqueId());
        //10 second immunity
        Scheduler.later(() -> this.combatRepository.unsetImmune(player.getUniqueId()), 20L * 10L);
    }

    public void handleBowShoot(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        Claim claim = this.claimRepository.getCachedClaim(player.getLocation());
        if (claim != null && !this.claimService.hasAccess(player, claim)) {
            event.setCancelled(true);
        }
    }
}
