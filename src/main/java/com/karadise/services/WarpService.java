package com.karadise.services;

import com.google.auto.service.AutoService;
import com.google.common.base.Joiner;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.repos.WarpRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.entities.position.WorldPosition;
import com.rhonim.toolbox.injection.Service;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

@Singleton
@AutoService(Service.class)
public class WarpService implements Service {
    private final WarpRepository warpRepository;

    @Inject
    public WarpService(WarpRepository warpRepository) {
        this.warpRepository = warpRepository;
    }

    @Override
    public void init() {
        this.warpRepository.init();
    }

    public void createWarp(String name, WorldPosition position) {
        this.warpRepository.createWarp(name, position);
    }

    public void deleteWarp(String name) {
        this.warpRepository.deleteWarp(name);
    }

    public void handleWarpCommand(CommandSender sender, String[] args) {
        Audience audience = Toolbox.getAudiences().sender(sender);
        if (args.length < 1) {
            audience.sendMessage(Identity.nil(), Component.text(">> Warps", NamedTextColor.GRAY));
            if (this.warpRepository.getWarpMap().size() <= 0) {
                audience.sendMessage(Identity.nil(), Component.text("> ", NamedTextColor.GRAY).append(Component.text("None", NamedTextColor.RED)));
            } else {
                if (this.warpRepository.getWarpMap().size() > 0 && sender instanceof Player) {
                    sender.sendMessage(Joiner.on(", ").join(this.warpRepository.getWarpMap().keySet().stream().map(StringUtils::capitalize).collect(Collectors.toList())));
                }
            }
            return;
        }

        Player player = (Player) sender;

        WorldPosition warp = this.warpRepository.getWarpMap().get(args[0].toLowerCase());
        if (warp == null) {
            audience.sendMessage(Identity.nil(), Component.text("Warp not found.", NamedTextColor.RED));
            return;
        }
        audience.sendMessage(Identity.nil(), Component.text("Whoosh!", NamedTextColor.GREEN));
        player.teleport(warp.toLocation());
    }
}
