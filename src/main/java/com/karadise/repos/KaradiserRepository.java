package com.karadise.repos;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.HomeWarp;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.rhonim.toolbox.database.mysql.MySQL;
import com.rhonim.toolbox.entities.position.WorldPosition;
import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;
import org.apache.cayenne.query.SelectById;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class KaradiserRepository {

    private final MySQL mySQL;

    private final Map<Long, Karadiser> idKaradiserMap = new ConcurrentHashMap<>();
    private final Map<UUID, Karadiser> uuidKaradiserMap = new ConcurrentHashMap<>();

    @Inject
    public KaradiserRepository(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public Karadiser getKaradiserDataNoCache(long playerID) {
        return Cayenne.objectForPK(this.mySQL.getNewContext(), Karadiser.class, playerID);
    }

    public Karadiser getKaradiserData(long playerID) {
        return ObjectSelect.query(Karadiser.class)
                .prefetch(Karadiser.CLAIM_MEMBER_LIST.disjointById())
                .prefetch(Karadiser.HOME.disjointById())
                .localCache()
                .where(Karadiser.PLAYER_ID.eq(playerID))
                .selectFirst(this.mySQL.getNewContext());
    }

    public Karadiser getKaradiserFromName(String name) {
        PlayerData data = ObjectSelect.query(PlayerData.class)
                .where(PlayerData.USERNAME.like(name))
                .selectFirst(this.mySQL.getNewContext());
        return data == null ? null : data.getKaradiser();
    }

    public void createKaradiserData(long playerID) {
        ObjectContext context = this.mySQL.getNewContext();

        PlayerData data = Cayenne.objectForPK(context, PlayerData.class, playerID);

        Karadiser karadiser = context.newObject(Karadiser.class);
        karadiser.setPlayerData(data);
        karadiser.save();

        this.cacheKaradiser(this.getKaradiserData(playerID));
    }

    public void createHome(long karadiserID, WorldPosition position) {
        ObjectContext context = this.mySQL.getNewContext();
        Karadiser karadiser = SelectById.query(Karadiser.class, karadiserID).selectFirst(context);
        HomeWarp warp = context.newObject(HomeWarp.class);
        warp.setKaradiser(karadiser);
        warp.setPosition(position);
        warp.save();
    }

    public void cacheKaradiser(Karadiser karadiser) {
        this.uuidKaradiserMap.put(karadiser.getPlayerData().getUuid(), karadiser);
        this.idKaradiserMap.put(karadiser.getId(), karadiser);
    }

    public void unloadKaradiserData(UUID uuid) {
        Karadiser karadiser = this.uuidKaradiserMap.remove(uuid);
        if (karadiser != null) {
            this.idKaradiserMap.remove(karadiser.getId());
        }
    }

    public Karadiser getCachedKaradiser(UUID uuid) {
        return this.uuidKaradiserMap.get(uuid);
    }

    public Karadiser getCachedKaradiser(long id) {
        return this.idKaradiserMap.get(id);
    }
}
