package com.karadise.repos;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Warp;
import com.rhonim.toolbox.database.mysql.MySQL;
import com.rhonim.toolbox.entities.position.WorldPosition;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class WarpRepository {
    private final MySQL mySQL;

    private final Map<String, WorldPosition> warpMap = new HashMap<>();

    @Inject
    public WarpRepository(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public void init() {
        List<Warp> warps = ObjectSelect.query(Warp.class).select(this.mySQL.getNewContext());
        for (Warp w : warps) {
            this.warpMap.put(w.getName().toLowerCase(), w.getWorldPosition());
        }
    }

    public void createWarp(String name, WorldPosition position) {
        ObjectContext context = this.mySQL.getNewContext();

        Warp warp = context.newObject(Warp.class);
        warp.setName(name);
        warp.setWorldPosition(position);
        warp.save();

        this.warpMap.put(name.toLowerCase(), position);
    }


    public void deleteWarp(String name) {
        Warp warp = ObjectSelect.query(Warp.class).where(Warp.NAME.eq(name)).selectFirst(this.mySQL.getNewContext());
        if (warp != null) {
            warp.delete();
        }
        this.warpMap.remove(name);
    }

    public Map<String, WorldPosition> getWarpMap() {
        return warpMap;
    }
}
