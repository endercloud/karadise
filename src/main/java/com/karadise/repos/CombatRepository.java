package com.karadise.repos;

import com.google.inject.Singleton;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Singleton
public class CombatRepository {
    private final Set<UUID> immunePlayers = new HashSet<>();

    public boolean isImmune(UUID uuid) {
        return this.immunePlayers.contains(uuid);
    }

    public void setImmune(UUID uuid) {
        this.immunePlayers.add(uuid);
    }

    public void unsetImmune(UUID uuid) {
        this.immunePlayers.remove(uuid);
    }
}
