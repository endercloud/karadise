package com.karadise.repos;

import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.PlayerData;
import com.rhonim.toolbox.database.mysql.MySQL;
import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;
import org.bukkit.entity.Player;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class PlayerRepository {

    private final MySQL mySQL;

    private final Map<UUID, PlayerData> playerDataMap = new ConcurrentHashMap<>();
    private final Map<Long, PlayerData> playerDataIdMap = new ConcurrentHashMap<>();

    @Inject
    public PlayerRepository(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public PlayerData getCachedPlayerData(long playerID) {
        return this.playerDataIdMap.get(playerID);
    }

    public PlayerData getCachedPlayerData(UUID uuid) {
        return this.playerDataMap.get(uuid);
    }

    public PlayerData getPlayerData(long playerId) {
        return Cayenne.objectForPK(this.mySQL.getRuntime().newContext(), PlayerData.class, playerId);
    }

    public PlayerData getPlayerData(UUID uuid) {
        return ObjectSelect.query(PlayerData.class)
                .where(PlayerData.UUID.eq(uuid))
                .selectFirst(this.mySQL.getRuntime().newContext());
    }

    public PlayerData getPlayerData(String name) {
        return ObjectSelect.query(PlayerData.class)
                .where(PlayerData.USERNAME.like(name))
                .selectFirst(this.mySQL.getRuntime().newContext());
    }

    public PlayerData createPlayerData(Player player) {
        ObjectContext context = this.mySQL.getRuntime().newContext();
        PlayerData playerData = context.newObject(PlayerData.class);
        playerData.setUsername(player.getName());
        playerData.setUuid(player.getUniqueId());
        playerData.setSkin(this.getPlayerSkinURL(player));
        playerData.setFirstSeen(LocalDateTime.now());
        playerData.setLastSeen(LocalDateTime.now());
        playerData.save();

        return playerData;
    }


    public void updateName(long playerID, String name) {
        PlayerData playerData = Cayenne.objectForPK(this.mySQL.getRuntime().newContext(), PlayerData.class, playerID);
        if (playerData == null) {
            return;
        }

        playerData.setUsername(name);
        playerData.save();
    }

    public void updateSkin(long playerID, String skin) {
        PlayerData playerData = Cayenne.objectForPK(this.mySQL.getRuntime().newContext(), PlayerData.class, playerID);
        if (playerData == null) {
            return;
        }

        playerData.setSkin(skin);
        playerData.save();
    }

    public void updateNameSkinLast(long playerID, String name, String skin) {
        PlayerData playerData = Cayenne.objectForPK(this.mySQL.getRuntime().newContext(), PlayerData.class, playerID);
        if (playerData == null) {
            return;
        }

        this.updateNameSkinLast(playerData, name, skin);
    }

    public void updateNameSkinLast(PlayerData playerData, String name, String skin) {
        playerData.setUsername(name);
        playerData.setSkin(skin);
        playerData.setLastSeen(LocalDateTime.now());
        playerData.save();
    }

    public void cachePlayerData(UUID uuid) {
        if (this.playerDataMap.get(uuid) != null) {
            return;
        }
        PlayerData data = this.getPlayerData(uuid);
        this.cachePlayerData(data);
    }

    public void cachePlayerData(PlayerData data) {
        this.playerDataMap.put(data.getUuid(), data);
        this.playerDataIdMap.put(data.getId(), data);
    }

    public void unloadPlayerData(UUID uuid) {
        PlayerData playerData;
        if ((playerData = this.playerDataMap.remove(uuid)) != null) {
            this.playerDataIdMap.remove(playerData.getId());
        }
    }

    public String getPlayerSkinURL(Player player) {
        String skinURL = "";
        for (ProfileProperty profileProperty : player.getPlayerProfile().getProperties()) {
            if (profileProperty.getName().equals("textures")) {
                JsonObject jsonObject = new JsonParser().parse(new String(Base64.getDecoder().decode(profileProperty.getValue()))).getAsJsonObject();
                if (jsonObject.has("textures")) {
                    JsonObject texObj = jsonObject.getAsJsonObject("textures");
                    if (texObj.has("SKIN")) {
                        JsonObject skinObj = texObj.getAsJsonObject("SKIN");
                        if (skinObj.has("url")) {
                            skinURL = skinObj.getAsJsonPrimitive("url").getAsString();
                        }
                    }
                }
            }
        }

        return skinURL;
    }
}
