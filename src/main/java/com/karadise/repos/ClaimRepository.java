package com.karadise.repos;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.*;
import com.karadise.enums.ClaimType;
import com.rhonim.toolbox.database.mysql.MySQL;
import com.rhonim.toolbox.entities.position.WorldPosition;
import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class ClaimRepository {

    /**
     * Key is chunk X, chunk Y as a String
     */
    private final Map<String, Claim> claimChunkMap = new ConcurrentHashMap<>();
    private final Map<String, Long> claimOwnerMap = new ConcurrentHashMap<>();
    private final Map<Long, Claim> claimIdMap = new ConcurrentHashMap<>();
    /*
    Uses karadiser ID
     */
    private final Map<Long, Map<Long, PlayerData>> globalMembers = new ConcurrentHashMap<>();

    private final MySQL mySQL;

    @Inject
    public ClaimRepository(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public void init() {
        List<Claim> claims = ObjectSelect.query(Claim.class)
                .prefetch(Claim.CLAIM_MEMBERS.disjointById())
                .localCache()
                .select(this.mySQL.getNewContext());

        for (Claim c : claims) {
            //Give each claim its own context so we only save that claim at one time :)
            c.setObjectContext(this.mySQL.getNewContext());
            System.out.println("Caching claim at " + c.getChunkX() + ", " + c.getChunkZ() + " with map key \"" + this.getMapKey(c) +"\"");
            this.claimIdMap.put(c.getId(), c);
            this.claimChunkMap.put(this.getMapKey(c), c);
            this.claimOwnerMap.put(this.getMapKey(c), c.getOwnerId());
        }

        List<GlobalClaimMember> globalMemberShips = ObjectSelect.query(GlobalClaimMember.class)
                .select(this.mySQL.getNewContext());

        for (GlobalClaimMember cgm : globalMemberShips) {
            Map<Long, PlayerData> map;
            if ((map = this.globalMembers.get(cgm.getOwnerId())) == null) {
                map = new HashMap<>();
                this.globalMembers.put(cgm.getOwnerId(), map);
            }

            ObjectContext context = this.mySQL.getNewContext();
            Karadiser karadiser = Cayenne.objectForPK(context, Karadiser.class, cgm.getMemberId());
            PlayerData data = ObjectSelect.query(PlayerData.class).where(PlayerData.KARADISER.eq(karadiser)).selectFirst(context);
            map.put(cgm.getMemberId(), data);
        }
    }

    public Claim createClaim(long ownerID, String ownerName, ClaimType type, String world, long chunkX, long chunkZ) {
        ObjectContext context = this.mySQL.getNewContext();

        Karadiser karadiser = Cayenne.objectForPK(context, Karadiser.class, ownerID);

        Claim claim = context.newObject(Claim.class);
        claim.setChunkX(chunkX);
        claim.setChunkZ(chunkZ);
        claim.setCachedOwnerName(ownerName);
        claim.setClaimType(type);
        claim.setOwner(karadiser);
        claim.setOwnerId(karadiser.getId());
        claim.setWorld(world);
        claim.save();

        this.claimIdMap.put(claim.getId(), claim);
        this.claimChunkMap.put(this.getMapKey(claim), claim);
        this.claimOwnerMap.put(this.getMapKey(claim), ownerID);

        return claim;
    }

    public void removeClaim(long claimID) {
        ObjectContext context = this.mySQL.getNewContext();
        Claim claim = Cayenne.objectForPK(context, Claim.class, claimID);
        context.deleteObjects(claim.getClaimMembers());

        this.claimIdMap.remove(claimID);
        this.claimChunkMap.remove(this.getMapKey(claim));
        this.claimOwnerMap.remove(this.getMapKey(claim));

        claim.delete();
    }

    public void createClaimMembership(long claimID, long memberID) {
        ObjectContext context = this.mySQL.getNewContext();
        Claim claim = Cayenne.objectForPK(context, Claim.class, claimID);
        Karadiser karadiser = Cayenne.objectForPK(context, Karadiser.class, memberID);

        ClaimMember member = context.newObject(ClaimMember.class);
        member.setClaim(claim);
        member.setKaradiser(karadiser);
        member.save();
    }

    public void removeClaimMembership(long claimID, long memberID) {
        ObjectContext context = this.mySQL.getNewContext();
        Claim claim = Cayenne.objectForPK(context, Claim.class, claimID);
        Karadiser karadiser = Cayenne.objectForPK(context, Karadiser.class, memberID);

        ClaimMember member = ObjectSelect.query(ClaimMember.class)
                .where(ClaimMember.CLAIM.eq(claim)).and(ClaimMember.KARADISER.eq(karadiser))
                .selectFirst(context);
        if (member != null) {
            member.delete();
        }
    }

    public Claim getCachedClaim(long id) {
        return this.claimIdMap.get(id);
    }

    public Claim getCachedClaim(String worldName, long x, long z) {
        return this.claimChunkMap.get(worldName + ":" + x + "," + z);
    }

    public Claim getCachedClaim(WorldPosition position) {
        return this.claimChunkMap.get(this.getMapKey(position));
    }

    public Claim getCachedClaim(Location location) {
        return this.claimChunkMap.get(this.getMapKey(location));
    }

    public void addCachedClaimOwner(Location location, long ownerID) {
        this.claimOwnerMap.put(this.getMapKey(location), ownerID);
    }

    public boolean isClaimed(Location location) {
        return this.claimOwnerMap.containsKey(this.getMapKey(location));
    }

    public void updateOwnerCachedName(long id, String name) {
        Claim claim = Cayenne.objectForPK(this.mySQL.getNewContext(), Claim.class, id);
        claim.setCachedOwnerName(name);
        claim.save();
    }

    public void createGlobalMembership(long ownerID, long memberID) {
        ObjectContext context = this.mySQL.getNewContext();
        GlobalClaimMember gcm = context.newObject(GlobalClaimMember.class);
        gcm.setOwnerId(ownerID);
        gcm.setMemberId(memberID);
        gcm.save();
    }

    public void removeGlobalMembershop(long ownerID, long memberID) {
        GlobalClaimMember member = ObjectSelect.query(GlobalClaimMember.class)
                .where(GlobalClaimMember.OWNER_ID.eq(ownerID).andExp(GlobalClaimMember.MEMBER_ID.eq(memberID)))
                .selectFirst(this.mySQL.getNewContext());
        if (member != null) {
            member.delete();
        }
    }

    public void uncacheGlobalMembership(long ownerID, long memberID) {
        Map<Long, PlayerData> map;
        if ((map = this.globalMembers.get(ownerID)) == null) {
            return;
        }
        map.remove(memberID);
    }

    public void cacheGlobalMembership(long ownerID, long memberID, PlayerData data) {
        Map<Long, PlayerData> map;
        if ((map = this.globalMembers.get(ownerID)) == null) {
            map = new ConcurrentHashMap<>();
            this.globalMembers.put(ownerID, map);
        }

        map.put(memberID, data);
    }

    public ImmutableMap<Long, PlayerData> getGlobalMemberships(long ownerID) {
        Map<Long, PlayerData> map = this.globalMembers.get(ownerID);
        return map == null ? ImmutableMap.of() : ImmutableMap.copyOf(map);
    }

    public Map<Long, Map<Long, PlayerData>> getGlobalMemberships() {
        return ImmutableMap.copyOf(this.globalMembers);
    }

    private String getMapKey(Claim c) {
        return c.getWorld() + ":" + c.getChunkX() + "," + c.getChunkZ();
    }

    private String getMapKey(WorldPosition position) {
        return position.getWorld() + ":" + position.getChunkX() + "," + position.getChunkZ();
    }

    private String getMapKey(Location loc) {
        return loc.getWorld().getName() + ":" + (loc.getBlockX() >> 4) + "," + (loc.getBlockZ() >> 4);
    }
}
