package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Singleton
@AutoService(ToolboxCommand.class)
public class DiscordCommand extends ToolboxCommand {

    @Inject
    public DiscordCommand() {
        super("discord");
    }

    @Override
    public boolean run(CommandSender sender, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Toolbox.getAudiences().sender(sender).sendMessage(Identity.nil(), Component.text("[Click to join Discord]", NamedTextColor.AQUA, TextDecoration.BOLD, TextDecoration.UNDERLINED)
                .hoverEvent(HoverEvent.showText(Component.text("Go to: ").append(Component.text("https://discord.com/invite/fY2v28C", NamedTextColor.AQUA, TextDecoration.UNDERLINED))))
                .clickEvent(ClickEvent.openUrl("https://discord.com/invite/fY2v28C")));

        return true;
    }
}
