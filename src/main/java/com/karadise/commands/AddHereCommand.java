package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.entities.gui.PlayerSelectorGUI;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.database.AsyncDatabase;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@Singleton
@AutoService(ToolboxCommand.class)
public class AddHereCommand extends ToolboxCommand {

    private final Injector injector;
    private final ClaimRepository claimRepository;
    private final PlayerRepository playerRepository;
    private final KaradiserRepository karadiserRepository;

    @Inject
    public AddHereCommand(Injector injector, ClaimRepository claimRepository, PlayerRepository playerRepository, KaradiserRepository karadiserRepository) {
        super("addhere");
        this.injector = injector;
        this.claimRepository = claimRepository;
        this.playerRepository = playerRepository;
        this.karadiserRepository = karadiserRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;
        Audience audience = Toolbox.getAudiences().player(player);

        final Claim currentClaim = this.claimRepository.getCachedClaim(player.getLocation());
        if (currentClaim == null) {
            audience.sendMessage(Identity.nil(), Component.text("There is no claim here!", NamedTextColor.RED));
            return true;
        }

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());

        if (currentClaim.getOwnerId() != karadiser.getId()) {
            audience.sendMessage(Identity.nil(), Component.text("You are not the owner of this claim!", NamedTextColor.RED));
            return true;
        }

        if (args.length > 0) {
            Player p = Bukkit.getPlayer(args[0]);
            if (p == null) {
                audience.sendMessage(Identity.nil(), Component.text("That player is not online!"));
                return true;
            }

            Karadiser target = this.karadiserRepository.getCachedKaradiser(p.getUniqueId());

            if (target.getId() == currentClaim.getOwnerId()) {
                audience.sendMessage(Identity.nil(), Component.text("Already a member!", NamedTextColor.RED));
                return true;
            }
            if (!currentClaim.isMember(target)) {
                AsyncDatabase.submit(() -> this.claimRepository.createClaimMembership(currentClaim.getId(), target.getId()));
            }
            audience.sendMessage(Identity.nil(), Component.text("Added \"" + p.getName() +"\" to this claim!", NamedTextColor.GREEN));
            return true;
        }

        Map<Long, PlayerData> players = new HashMap<>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getUniqueId().equals(((Player) sender).getUniqueId())) {
                continue;
            }
            Karadiser k = this.karadiserRepository.getCachedKaradiser(p.getUniqueId());
            if (currentClaim.isMember(k)) {
                continue;
            }
            PlayerData data = this.playerRepository.getCachedPlayerData(p.getUniqueId());
            players.put(k.getId(), data);
        }

        this.injector.getInstance(PlayerSelectorGUI.class).open((Player) sender, "Add player to this claim", players, karadiserID -> {
            Karadiser target = this.karadiserRepository.getCachedKaradiser(karadiserID);
            if (target == null) {
                audience.sendMessage(Identity.nil(), Component.text("Player not found.", NamedTextColor.RED));
                return;
            }
            PlayerData targetPlayerData = this.playerRepository.getCachedPlayerData(target.getPlayerId());

            AsyncDatabase.submit(() -> {
                this.claimRepository.createClaimMembership(currentClaim.getId(), target.getId());
                audience.sendMessage(Identity.nil(), Component.text("Added \"" + targetPlayerData.getUsername() +"\" to this claim!", NamedTextColor.GREEN));
            });
        });
        return true;
    }
}
