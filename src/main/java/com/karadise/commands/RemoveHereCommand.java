package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.ClaimMember;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.entities.gui.PlayerSelectorGUI;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.database.AsyncDatabase;
import com.rhonim.toolbox.util.Scheduler;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@Singleton
@AutoService(ToolboxCommand.class)
public class RemoveHereCommand extends ToolboxCommand {

    private final Injector injector;
    private final ClaimRepository claimRepository;
    private final KaradiserRepository karadiserRepository;
    private final PlayerRepository playerRepository;

    @Inject
    public RemoveHereCommand(Injector injector, ClaimRepository claimRepository, KaradiserRepository karadiserRepository, PlayerRepository playerRepository) {
        super("removehere");
        this.injector = injector;
        this.claimRepository = claimRepository;
        this.karadiserRepository = karadiserRepository;
        this.playerRepository = playerRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        Audience audience = Toolbox.getAudiences().player(player);
        final Claim currentClaim = this.claimRepository.getCachedClaim(player.getLocation());
        if (currentClaim == null) {
            audience.sendMessage(Identity.nil(), Component.text("There is no claim here!", NamedTextColor.RED));
            return true;
        }

        Karadiser owner = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
        if (currentClaim.getOwnerId() != owner.getId()) {
            audience.sendMessage(Identity.nil(), Component.text("You are not the owner of this claim!", NamedTextColor.RED));
            return true;
        }

        if (args.length > 0) {
            Scheduler.async(() -> {

                Karadiser tar = null;
                PlayerData data = null;
                for (ClaimMember cm : currentClaim.getClaimMembers()) {
                    Karadiser k = cm.getKaradiser();
                    data = k.getPlayerData();
                    if (data.getUsername().equalsIgnoreCase(args[0])) {
                        tar = k;
                        break;
                    }
                }

                if (tar == null) {
                    audience.sendMessage(Identity.nil(), Component.text("This is not a valid player!", NamedTextColor.RED));
                    return;
                }

                this.claimRepository.removeClaimMembership(currentClaim.getId(), tar.getId());
                audience.sendMessage(Identity.nil(), Component.text("Removed \"" + data.getUsername() + "\" from this claim!", NamedTextColor.GREEN));

            });
            return true;
        }

        Scheduler.async(() -> {

            Map<Long, PlayerData> players = new HashMap<>();
            for (ClaimMember cm : currentClaim.getClaimMembers()) {
                Karadiser k = cm.getKaradiser();
                players.put(k.getId(), k.getPlayerData());
            }

            this.injector.getInstance(PlayerSelectorGUI.class).open((Player) sender, "Remove player from this claim", players, karadiserID -> {
                Karadiser target = this.karadiserRepository.getCachedKaradiser(karadiserID);
                if (target == null) {
                    audience.sendMessage(Identity.nil(), Component.text("Player not found.", NamedTextColor.RED));
                    return;
                }
                PlayerData data = this.playerRepository.getCachedPlayerData(target.getPlayerId());
                AsyncDatabase.submit(() -> {
                    this.claimRepository.removeClaimMembership(currentClaim.getId(), target.getId());
                    audience.sendMessage(Identity.nil(), Component.text("Removed \"" + data.getUsername() + "\" from this claim!", NamedTextColor.GREEN));
                });
            });
        });

        return true;
    }
}
