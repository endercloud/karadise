package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.repos.ClaimRepository;
import com.karadise.services.ClaimService;
import com.karadise.tasks.ChunkOutlineTask;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Singleton
@AutoService(ToolboxCommand.class)
public class ClaimOutlineCommand extends ToolboxCommand {
    private final ClaimService claimService;
    private final ClaimRepository claimRepository;

    @Inject
    public ClaimOutlineCommand(ClaimService claimService, ClaimRepository claimRepository) {
        super("claimoutline");
        this.claimService = claimService;
        this.claimRepository = claimRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        Audience audience = Toolbox.getAudiences().player(player);
        Claim claim = this.claimRepository.getCachedClaim(player.getLocation());
        if (claim == null) {
            audience.sendMessage(Identity.nil(), Component.text("There is no claim here!", NamedTextColor.RED));
            return true;
        } else {
            if (!claimService.hasAccess(player, claim)) {
                audience.sendMessage(Identity.nil(), Component.text("You must be a member of the current claim to do that!", NamedTextColor.RED));
                return true;
            }
        }

        new ChunkOutlineTask(player, player.getLocation().getChunk().getChunkSnapshot()).run();
        return true;
    }
}
