package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.WarpService;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.entities.position.WorldPosition;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;

@Singleton
@AutoService(ToolboxCommand.class)
public class AdminWarpCommand extends ToolboxCommand {
    @Inject
    private WarpService warpService;

    public AdminWarpCommand() {
        super("adminwarp");
        setPermission("karadise.command.adminwarp");
        registerSubCommands(
                new AddWarpCommand(),
                new DeleteWarpCommand()
        );
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        this.sendHelp(sender);
        return true;
    }


    public class AddWarpCommand extends ToolboxCommand {
        public AddWarpCommand() {
            super("add");
        }

        @Override
        public boolean run(CommandSender sender, String label, String[] args) {
            Audience audience = Toolbox.getAudiences().sender(sender);
            if (args.length < 1) {
                audience.sendMessage(Identity.nil(), Component.text("/adminwarp add <name>", NamedTextColor.RED));
                return true;
            }

            Player player = (Player) sender;
            warpService.createWarp(args[0], new WorldPosition(player.getLocation()));
            audience.sendMessage(Identity.nil(), Component.text("Created.", NamedTextColor.GREEN));
            return true;
        }
    }

    public class DeleteWarpCommand extends ToolboxCommand {
        public DeleteWarpCommand() {
            super("delete");
            setAliases(Collections.singletonList("del"));
        }

        @Override
        public boolean run(CommandSender sender, String label, String[] args) {
            Audience audience = Toolbox.getAudiences().sender(sender);
            if (args.length < 1) {
                audience.sendMessage(Identity.nil(), Component.text("/adminwarp delete <name>", NamedTextColor.RED));
                return true;
            }

            warpService.deleteWarp(args[0]);
            audience.sendMessage(Identity.nil(), Component.text("Deleted.", NamedTextColor.GREEN));
            return true;
        }
    }
}
