package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.enums.ClaimType;
import com.karadise.services.ClaimService;
import com.rhonim.toolbox.commands.ToolboxCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Singleton
@AutoService(ToolboxCommand.class)
public class FarmClaimCommand extends ToolboxCommand {

    private final ClaimService claimService;

    @Inject
    public FarmClaimCommand(ClaimService claimService) {
        super("farmclaim");
        this.claimService = claimService;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        this.claimService.doClaim((Player) sender, ClaimType.FARM);

        return true;
    }
}
