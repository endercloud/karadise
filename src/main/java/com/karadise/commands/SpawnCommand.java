package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.WarpService;
import com.rhonim.toolbox.commands.ToolboxCommand;
import org.bukkit.command.CommandSender;

@Singleton
@AutoService(ToolboxCommand.class)
public class SpawnCommand extends ToolboxCommand {
    private final WarpService warpService;

    @Inject
    public SpawnCommand(WarpService warpService) {
        super("spawn");
        this.warpService = warpService;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        this.warpService.handleWarpCommand(sender, new String[] {"spawn"});
        return true;
    }
}
