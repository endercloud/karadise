package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.PlayerService;
import com.rhonim.toolbox.commands.ToolboxCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Singleton
@AutoService(ToolboxCommand.class)
public class ToggleShoutCommand extends ToolboxCommand{

    private final PlayerService playerService;

    @Inject
    public ToggleShoutCommand(PlayerService playerService) {
        super("toggleshout");
        this.playerService = playerService;
    }

    @Override
    public boolean run(CommandSender sender, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        playerService.handleShoutToggle((Player) sender);
        return true;
    }
}
