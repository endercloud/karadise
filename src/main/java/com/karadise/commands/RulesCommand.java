package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.gson.JsonArray;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.io.InputStreamReader;

@Singleton
@AutoService(ToolboxCommand.class)
public class RulesCommand extends ToolboxCommand {

    public static final Book RULES_BOOK;

    static {
        Book.Builder book = Book.builder()
                .title(Component.text("Karadise v4", TextColor.color(253, 189, 255)))
                .author(Component.text("Pwincessly"));

        try (InputStreamReader reader = new InputStreamReader(RulesCommand.class.getResourceAsStream("/book.json"))) {
            JsonArray pages = GsonComponentSerializer.gson().serializer().fromJson(reader, JsonArray.class);
            pages.forEach(page -> book.addPage(GsonComponentSerializer.gson().serializer().fromJson(page, Component.class)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        RULES_BOOK = book.build();
    }

    @Inject
    public RulesCommand() {
        super("rules");
    }

    @Override
    public boolean run(CommandSender sender, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Audience audience = Toolbox.getAudiences().player((Player) sender);
        audience.openBook(RULES_BOOK);
        return true;
    }
}
