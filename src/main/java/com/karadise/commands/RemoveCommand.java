package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.ClaimMember;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.entities.gui.PlayerSelectorGUI;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.database.AsyncDatabase;
import com.rhonim.toolbox.util.Scheduler;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@Singleton
@AutoService(ToolboxCommand.class)
public class RemoveCommand extends ToolboxCommand {

    private final Injector injector;
    private final KaradiserRepository karadiserRepository;
    private final PlayerRepository playerRepository;
    private final ClaimRepository claimRepository;

    @Inject
    public RemoveCommand(Injector injector, KaradiserRepository karadiserRepository, PlayerRepository playerRepository, ClaimRepository claimRepository) {
        super("remove");
        this.injector = injector;
        this.karadiserRepository = karadiserRepository;
        this.playerRepository = playerRepository;
        this.claimRepository = claimRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        Audience audience = Toolbox.getAudiences().player(player);
        Karadiser owner = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());

        if (args.length > 0) {
            ImmutableMap<Long, PlayerData> map = this.claimRepository.getGlobalMemberships(owner.getId());
            long idToRemove = -1;
            for (Map.Entry<Long, PlayerData> entry : map.entrySet()) {
                if (entry.getValue().getUsername().equalsIgnoreCase(args[0])) {
                    idToRemove = entry.getKey();
                    break;
                }
            }


            final long finalID = idToRemove;
            AsyncDatabase.submit(() -> {
                long actualID = finalID;
                if (actualID == -1) {
                    for (Claim c : owner.getOwnedClaims().values()) {
                        Claim claim = this.claimRepository.getCachedClaim(c.getId());

                        for (ClaimMember member : claim.getClaimMembers()) {
                            if (member.getKaradiser().getPlayerData().getUsername().equalsIgnoreCase(args[0])) {
                                actualID = member.getKaradiserId();
                                break;
                            }
                        }
                    }
                }

                if (actualID == -1) {
                    audience.sendMessage(Identity.nil(), Component.text("This is not a valid player!", NamedTextColor.RED));
                    return;
                }

                this.claimRepository.uncacheGlobalMembership(owner.getId(), actualID);


                this.claimRepository.removeGlobalMembershop(owner.getId(), actualID);
                for (Claim claim : owner.getOwnedClaims().values()) {
                    if (claim.isMember(actualID)) {
                        this.claimRepository.removeClaimMembership(claim.getId(), actualID);
                    }
                }

                audience.sendMessage(Identity.nil(), Component.text("Removed \"" + args[0] + "\" from all claims!", NamedTextColor.GREEN));
            });

            return true;
        }


        Scheduler.async(() -> {

            Map<Long, PlayerData> players = new HashMap<>();
            for (Claim c : owner.getOwnedClaims().values()) {
                Claim claim = this.claimRepository.getCachedClaim(c.getId());
                for (ClaimMember cm : claim.getClaimMembers()) {
                    Karadiser k = cm.getKaradiser();
                    players.put(k.getId(), k.getPlayerData());
                }
            }

            players.putAll(this.claimRepository.getGlobalMemberships(owner.getId()));

            this.injector.getInstance(PlayerSelectorGUI.class).open((Player) sender, "Remove player from all claims", players, karadiserID -> {
                Karadiser target = this.karadiserRepository.getCachedKaradiser(karadiserID);
                if (target == null) {
                    AsyncDatabase.submit(() -> {

                        Karadiser t = this.karadiserRepository.getKaradiserDataNoCache(karadiserID);
                        if (t == null) {
                            audience.sendMessage(Identity.nil(), Component.text("Player not found.", NamedTextColor.RED));
                            return;
                        }
                        PlayerData targetPlayerData = t.getPlayerData();

                        this.claimRepository.uncacheGlobalMembership(owner.getId(), t.getId());
                        this.claimRepository.removeGlobalMembershop(owner.getId(), t.getId());

                        for (Claim c : owner.getOwnedClaims().values()) {
                            Claim claim = this.claimRepository.getCachedClaim(c.getId());
                            if (claim.isMember(t.getId())) {
                                this.claimRepository.removeClaimMembership(claim.getId(), t.getId());
                            }
                        }

                        audience.sendMessage(Identity.nil(), Component.text("Removed \"" + targetPlayerData.getUsername() + "\" from all claims!", NamedTextColor.GREEN));
                    });
                    return;
                }
                PlayerData targetPlayerData = this.playerRepository.getCachedPlayerData(target.getPlayerId());

                this.claimRepository.uncacheGlobalMembership(owner.getId(), target.getId());
                AsyncDatabase.submit(() -> {
                    this.claimRepository.removeGlobalMembershop(owner.getId(), target.getId());
                });

                audience.sendMessage(Identity.nil(), Component.text("Removed \"" + targetPlayerData.getUsername() + "\" from all claims!", NamedTextColor.GREEN));
            });
        });

        return true;
    }
}
