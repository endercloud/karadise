package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.ClaimMember;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.enums.ClaimType;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.karadise.services.ClaimService;
import com.karadise.tasks.ChunkOutlineTask;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.util.Scheduler;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Singleton
@AutoService(ToolboxCommand.class)
public class ClaimsCommand extends ToolboxCommand {
    private final PlayerRepository playerRepository;
    private final KaradiserRepository karadiserRepository;
    private final ClaimService claimService;
    private final ClaimRepository claimRepository;

    @Inject
    public ClaimsCommand(PlayerRepository playerRepository, KaradiserRepository karadiserRepository, ClaimService claimService, ClaimRepository claimRepository) {
        super("claims");
        this.playerRepository = playerRepository;
        this.karadiserRepository = karadiserRepository;
        this.claimService = claimService;
        this.claimRepository = claimRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        Audience audience = Toolbox.getAudiences().sender(sender);

        Scheduler.async(() -> {
            Karadiser player = null;

            if (args.length > 0) {
                if (!sender.hasPermission("karadise.commands.claims.lookup")) {
                    audience.sendMessage(Identity.nil(), Component.text("You can only look at the claims you are in.", NamedTextColor.RED));
                    return;
                }

                player = this.karadiserRepository.getKaradiserFromName(args[0]);
                if (player == null) {
                    audience.sendMessage(Identity.nil(), Component.text("Player not found.", NamedTextColor.RED));
                    return;
                }

            } else if (sender instanceof Player) {
                player = this.karadiserRepository.getCachedKaradiser(((Player) sender).getUniqueId());
            }

            if (player == null) {
                audience.sendMessage(Identity.nil(), Component.text("You must specify a player..", NamedTextColor.RED));
                return;
            }

            PlayerData data = this.playerRepository.getCachedPlayerData(player.getPlayerId());
            if (data == null) {
                data = player.getPlayerData();
            }

            audience.sendMessage(Identity.nil(), Component.text(">> ", NamedTextColor.GRAY)
                    .append(Component.text(data.getUsername() + "'s", NamedTextColor.YELLOW))
                    .append(Component.text(" claims", NamedTextColor.GRAY)));

            if (player.getOwnedClaims().isEmpty() && player.getClaimMemberList().isEmpty()) {
                audience.sendMessage(Identity.nil(), Component.text("> ", NamedTextColor.GRAY)
                        .append(Component.text("None!", NamedTextColor.RED)));
            } else {
                Set<Long> handledClaims = new HashSet<>();


                //Owned
                for (Object obj : player.getOwnedClaims().keySet()) {
                    long id = (long) obj;
                    Claim claim = this.claimRepository.getCachedClaim(id);
                    audience.sendMessage(Identity.nil(), getClaimLine(claim).append(Component.text(claim.getOwnerId() == player.getId() ? " (Owns)" : "", NamedTextColor.DARK_GRAY)));
                    handledClaims.add(id);
                }

                //Not Owned
                for (ClaimMember member : player.getClaimMemberList()) {
                    Claim claim = this.claimRepository.getCachedClaim(member.getClaimId());
                    audience.sendMessage(Identity.nil(), getClaimLine(claim));
                    handledClaims.add(claim.getId());
                }

                //Global
                for (Map.Entry<Long, Map<Long, PlayerData>> entry : this.claimRepository.getGlobalMemberships().entrySet()) {
                    long claimID = entry.getKey();
                    if (handledClaims.contains(claimID)){
                        continue;
                    }

                    Map<Long, PlayerData> map = entry.getValue();
                    if (map.containsKey(player.getId())) {
                        Claim claim = this.claimRepository.getCachedClaim(claimID);
                        if (claim == null) {
                            continue;
                        }

                        audience.sendMessage(Identity.nil(), getClaimLine(claim));
                    }
                }

                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    for (int x = -4; x <= 4; x++) {
                        for (int z = -4; z <= 4; z++) {
                            int chunkX = (p.getLocation().getBlockX() >> 4) + x;
                            int chunkZ = (p.getLocation().getBlockZ() >> 4) + z;

                            Claim claim = this.claimRepository.getCachedClaim(p.getWorld().getName(), chunkX, chunkZ);
                            if (claim != null && this.claimService.hasAccess(p, claim)) {
                               new ChunkOutlineTask(p, p.getWorld().getChunkAt(chunkX, chunkZ).getChunkSnapshot()).run();
                            }
                        }
                    }
                }

            }
        });


        return true;
    }

    private Component getClaimLine(Claim claim) {
        Component claimInfo = Component.text("> Chunk X: ", NamedTextColor.GRAY)
                .append(Component.text(String.valueOf(claim.getChunkX()), NamedTextColor.YELLOW))
                .append(Component.text(", Z: ", NamedTextColor.GRAY))
                .append(Component.text(String.valueOf(claim.getChunkZ()), NamedTextColor.YELLOW))
                .append(Component.text(", World: ", NamedTextColor.GRAY))
                .append(Component.text(claim.getWorld(), NamedTextColor.YELLOW));

        if(claim.getClaimType() != ClaimType.PERSONAL) {
            claimInfo.append(Component.text(" (" + claim.getClaimType() +")", NamedTextColor.YELLOW));
        }

        return claimInfo;
    }
}
