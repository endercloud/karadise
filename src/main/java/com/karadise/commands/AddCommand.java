package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.entities.database.generated.PlayerData;
import com.karadise.entities.gui.PlayerSelectorGUI;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.karadise.repos.PlayerRepository;
import com.karadise.services.ClaimService;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@Singleton
@AutoService(ToolboxCommand.class)
public class AddCommand extends ToolboxCommand {
    private final Injector injector;
    private final ClaimService claimService;
    private final KaradiserRepository karadiserRepository;
    private final PlayerRepository playerRepository;
    private final ClaimRepository claimRepository;

    @Inject
    public AddCommand(Injector injector, ClaimService claimService, KaradiserRepository karadiserRepository, PlayerRepository playerRepository, ClaimRepository claimRepository) {
        super("add");
        this.injector = injector;
        this.claimService = claimService;
        this.karadiserRepository = karadiserRepository;
        this.playerRepository = playerRepository;
        this.claimRepository = claimRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Karadiser owner = this.karadiserRepository.getCachedKaradiser(((Player) sender).getUniqueId());
        Audience audience = Toolbox.getAudiences().sender(sender);

        if (args.length > 0) {
            Player p = Bukkit.getPlayer(args[0]);

            if (p == null) {
                audience.sendMessage(Identity.nil(), Component.text("That player is not online!"));
                return true;
            }

            Karadiser target = this.karadiserRepository.getCachedKaradiser(p.getUniqueId());
            ImmutableMap<Long, PlayerData> existingGlobalMembers = this.claimRepository.getGlobalMemberships(owner.getId());
            if (!existingGlobalMembers.containsKey(target.getId())) {
                PlayerData targetPlayerData = this.playerRepository.getCachedPlayerData(p.getUniqueId());

                this.claimService.addGlobalMember(owner, target, targetPlayerData);
            }

            audience.sendMessage(Identity.nil(), Component.text("Added \"" + p.getName() + "\" to all claims!", NamedTextColor.GREEN));
            return true;
        }

        Map<Long, PlayerData> players = new HashMap<>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getUniqueId().equals(((Player) sender).getUniqueId())) {
                continue;
            }
            Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(p.getUniqueId());
            PlayerData data = this.playerRepository.getCachedPlayerData(p.getUniqueId());
            players.put(karadiser.getId(), data);
        }

        this.injector.getInstance(PlayerSelectorGUI.class).open((Player) sender, "Add player to all claims", players, karadiserID -> {
            Karadiser target = this.karadiserRepository.getCachedKaradiser(karadiserID);
            if (target == null) {
                audience.sendMessage(Identity.nil(), Component.text("Player not found.", NamedTextColor.RED));
                return;
            }
            PlayerData targetPlayerData = this.playerRepository.getCachedPlayerData(target.getPlayerId());

            this.claimService.addGlobalMember(owner, target, targetPlayerData);
        });

        return true;
    }
}
