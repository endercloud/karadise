package com.karadise.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.database.AsyncDatabase;
import com.rhonim.toolbox.util.PlayerUtil;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@Singleton
@AutoService(ToolboxCommand.class)
public class UnclaimCommand extends ToolboxCommand {

    private final ClaimRepository claimRepository;
    private final KaradiserRepository karadiserRepository;

    @Inject
    public UnclaimCommand(ClaimRepository claimRepository, KaradiserRepository karadiserRepository) {
        super("unclaim");
        this.claimRepository = claimRepository;
        this.karadiserRepository = karadiserRepository;
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        Audience audience = Toolbox.getAudiences().player(player);
        final Claim claim = this.claimRepository.getCachedClaim(player.getLocation());
        if (claim == null) {
            audience.sendMessage(Identity.nil(), Component.text("This is not a claim!", NamedTextColor.RED));
            return true;
        }

        Karadiser karadiser = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());

        if (claim.getOwnerId() != karadiser.getId() && !player.hasPermission("karadise.admin")) {
            audience.sendMessage(Identity.nil(), Component.text("This is not your claim!", NamedTextColor.RED));
            return true;
        }

        PlayerUtil.giveItemsSafely(player, new ItemStack(Material.IRON_BLOCK));
        AsyncDatabase.submit(() -> {
            this.claimRepository.removeClaim(claim.getId());
            audience.sendMessage(Identity.nil(), Component.text("Claim removed.", NamedTextColor.GREEN));
        });
        return true;
    }
}
