package com.karadise.enums;

public enum ClaimType {

    PERSONAL,
    SPAWNER,
    FARM

}
