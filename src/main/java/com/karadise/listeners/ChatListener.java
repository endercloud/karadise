package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.ChatService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

@Singleton
@AutoService(Listener.class)
public class ChatListener implements Listener {

    private final ChatService chatService;

    @Inject
    public ChatListener(ChatService chatService) {
        this.chatService = chatService;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        this.chatService.handleChatEvent(e);
    }
}
