package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.CombatService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

@Singleton
@AutoService(Listener.class)
public class DamageListener implements Listener {

    private final CombatService combatService;

    @Inject
    public DamageListener(CombatService combatService) {
        this.combatService = combatService;
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        this.combatService.handleDamage(event);
    }

    @EventHandler(ignoreCancelled = true)
    public void onDamageEntity(EntityDamageByEntityEvent event) {
        this.combatService.handleDamageEntity(event);
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        this.combatService.handleRespawn(event);
    }

    @EventHandler
    public void onPlayerShootProjectile(EntityShootBowEvent event) {
        this.combatService.handleBowShoot(event);
    }
}
