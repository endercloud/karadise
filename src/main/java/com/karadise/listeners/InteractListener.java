package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.ClaimService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;

@Singleton
@AutoService(Listener.class)
public class InteractListener implements Listener {
    private final ClaimService claimService;

    @Inject
    public InteractListener(ClaimService claimService) {
        this.claimService = claimService;
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {
        this.claimService.handleBucketEmpty(event);
    }

    @EventHandler
    public void onBucketFill(PlayerBucketFillEvent event) {
        this.claimService.handleBucketFill(event);
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent event) {
        this.claimService.handleHangingPlace(event);
    }

    @EventHandler
    public void onHangingBreak(HangingBreakByEntityEvent event) {
        this.claimService.handleHangingBreak(event);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        this.claimService.handleBlockInteract(event);
    }


    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        this.claimService.handleEntityInteract(event.getPlayer(), event.getRightClicked(), event);
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        this.claimService.handleEntityInteract(event.getPlayer(), event.getRightClicked(), event);
    }

    @EventHandler
    public void onPlayerEditArmorStand(PlayerArmorStandManipulateEvent event) {
        this.claimService.handleEntityInteract(event.getPlayer(), event.getRightClicked(), event);
    }

    @EventHandler
    public void onTame(EntityTameEvent event) {
        this.claimService.handleTame(event);
    }

}
