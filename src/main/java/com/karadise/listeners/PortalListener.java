package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.Karadise;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;

@Singleton
@AutoService(Listener.class)
public class PortalListener implements Listener {

    private final Karadise karadise;

    @Inject
    public PortalListener(Karadise karadise) {
        this.karadise = karadise;
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerUsePortal(PlayerPortalEvent event) {
        /*Player player = event.getPlayer();
        Audience audience = Toolbox.getAudiences().player(player);
        if (karadise.getConfig().getBoolean("end-disabled", true) && event.getTo().getWorld().getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName() + "_the_end")) {
            event.setCancelled(true);
            if (!player.hasMetadata("last-end-warn") || System.currentTimeMillis() - player.getMetadata("last-end-warn").get(0).asLong() >= TimeUnit.SECONDS.toMillis(10)) {
                player.setMetadata("last-end-warn",new FixedMetadataValue(karadise, System.currentTimeMillis()));
                audience.sendMessage(Identity.nil(), Component.text("The end is currently disabled!", NamedTextColor.RED));
            }
        }*/
    }
}
