package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.PlayerConnectionService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@Singleton
@AutoService(Listener.class)
public class ConnectionListener implements Listener {

    private final PlayerConnectionService playerConnectionService;

    @Inject
    public ConnectionListener(PlayerConnectionService playerConnectionService) {
        this.playerConnectionService = playerConnectionService;
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        this.playerConnectionService.handlePreLogin(event);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        this.playerConnectionService.handlePlayerLogin(e);
    }


    @EventHandler
    public void onKick(PlayerKickEvent e) {
        //this.playerConnectionService.handleKick(e.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.playerConnectionService.handleLogout(e);
    }
}
