package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.ClaimService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.world.StructureGrowEvent;

@Singleton
@AutoService(Listener.class)
public class BlockListener implements Listener {

    private final ClaimService claimService;

    @Inject
    public BlockListener(ClaimService claimService) {
        this.claimService = claimService;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        this.claimService.handleBlockBreak(event);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        this.claimService.handleBlockPlace(event);
    }


    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onBlockFromTo(BlockFromToEvent event) {
        this.claimService.handleWaterFlow(event);
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent event) {
        this.claimService.handleExplode(event);
    }

    @EventHandler
    public void onPistonPull(BlockPistonRetractEvent event) {
        this.claimService.handlePistonPull(event);
    }

    @EventHandler
    public void onPistonPush(BlockPistonExtendEvent event) {
        this.claimService.handlePistonPush(event);
    }

    @EventHandler
    public void onGrow(StructureGrowEvent event) {
        this.claimService.handleGrow(event);
    }


    @EventHandler
    public void onDispense(BlockDispenseEvent event) {
        this.claimService.handleDispense(event);
    }

    @EventHandler
    public void onIgnite(BlockIgniteEvent event) {
        this.claimService.handleIgnite(event);
    }
}
