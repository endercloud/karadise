package com.karadise.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.services.ClaimService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;

@Singleton
@AutoService(Listener.class)
public class VehicleListener implements Listener {

    private final ClaimService claimService;

    @Inject
    public VehicleListener(ClaimService claimService) {
        this.claimService = claimService;
    }

    @EventHandler
    public void onVehiclePush(VehicleEntityCollisionEvent event) {
        this.claimService.handleVehicle(event.getEntity(), event.getVehicle(), event);
    }

    @EventHandler
    public void onVehicleEnter(VehicleEnterEvent event) {
        this.claimService.handleVehicle(event.getEntered(), event.getVehicle(), event);
    }

    @EventHandler
    public void onVehicle(VehicleDamageEvent event) {
        this.claimService.handleVehicle(event.getAttacker(), event.getVehicle(), event);
    }
}
