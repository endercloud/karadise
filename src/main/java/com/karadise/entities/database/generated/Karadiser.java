package com.karadise.entities.database.generated;

import com.karadise.entities.database.generated.auto._Karadiser;

public class Karadiser extends _Karadiser {

    private static final long serialVersionUID = 1L;

    private boolean isShouting = false;
    private Claim currentClaim = null;

    public Claim getCurrentClaim() {
        return currentClaim;
    }

    public void setCurrentClaim(Claim currentClaim) {
        this.currentClaim = currentClaim;
    }

    public void setShouting(boolean shouting) {
        isShouting = shouting;
    }

    public boolean isShouting() {
        return isShouting;
    }
}
