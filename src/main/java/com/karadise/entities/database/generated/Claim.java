package com.karadise.entities.database.generated;

import com.karadise.entities.database.generated.auto._Claim;

public class Claim extends _Claim {

    private static final long serialVersionUID = 1L; 

    public boolean isMember(Karadiser karadiser) {
        return this.isMember(karadiser.getId());
    }

    public boolean isMember(long karadiserID) {
        for (ClaimMember cm : this.getClaimMembers()) {
            if (cm.getKaradiserId() == karadiserID) {
                return true;
            }
        }

        return false;
    }
}
