package com.karadise.entities.database;

import org.apache.cayenne.Cayenne;
import org.apache.cayenne.CayenneDataObject;

import java.util.Objects;

public class KaradiseDataObject extends CayenneDataObject {

    public long getId() {
        return Cayenne.longPKForObject(this);
    }

    public void save() {
        this.getObjectContext().commitChanges();
    }

    public void delete() {
        this.getObjectContext().deleteObject(this);
        this.getObjectContext().commitChanges();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass()) return false;
        return Objects.equals(((KaradiseDataObject)obj).getId(), this.getId());
    }
}