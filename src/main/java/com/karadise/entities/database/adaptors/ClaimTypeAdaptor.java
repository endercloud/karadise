package com.karadise.entities.database.adaptors;

import com.google.auto.service.AutoService;
import com.google.inject.Singleton;
import com.karadise.enums.ClaimType;
import org.apache.cayenne.access.types.ExtendedType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Singleton
@AutoService(ExtendedType.class)
public class ClaimTypeAdaptor implements ExtendedType<ClaimType> {
    @Override
    public String getClassName() {
        return ClaimType.class.getName();
    }

    @Override
    public void setJdbcObject(PreparedStatement statement, ClaimType claimType, int pos, int type, int scale) throws Exception {
        if (claimType == null) {
            statement.setNull(pos, type);
        } else {
            statement.setInt(pos, claimType.ordinal());
        }
    }

    @Override
    public ClaimType materializeObject(ResultSet rs, int index, int type) throws Exception {
        return ClaimType.values()[rs.getInt(index)];
    }

    @Override
    public ClaimType materializeObject(CallableStatement rs, int index, int type) throws Exception {
        return ClaimType.values()[rs.getInt(index)];
    }

    @Override
    public String toString(ClaimType claimtype) {
        return claimtype == null ? "NULL" : claimtype.name();
    }
}
