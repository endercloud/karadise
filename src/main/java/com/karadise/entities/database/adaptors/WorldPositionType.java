package com.karadise.entities.database.adaptors;

import com.google.auto.service.AutoService;
import com.google.inject.Singleton;
import com.rhonim.toolbox.entities.position.WorldPosition;
import org.apache.cayenne.access.types.ExtendedType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Singleton
@AutoService(ExtendedType.class)
public class WorldPositionType implements ExtendedType<WorldPosition> {
    @Override
    public String getClassName() {
        return WorldPosition.class.getName();
    }

    @Override
    public void setJdbcObject(PreparedStatement statement, WorldPosition position, int pos, int type, int scale) throws Exception {
        if (position == null) {
            statement.setNull(pos, type);
        } else {
            statement.setString(pos, position.toString());
        }
    }

    @Override
    public WorldPosition materializeObject(ResultSet rs, int index, int type) throws Exception {
        return new WorldPosition(rs.getString(index));
    }

    @Override
    public WorldPosition materializeObject(CallableStatement rs, int index, int type) throws Exception {
        return new WorldPosition(rs.getString(index));
    }

    @Override
    public String toString(WorldPosition position) {
        return position == null ? "NULL" : position.toString();
    }
}
