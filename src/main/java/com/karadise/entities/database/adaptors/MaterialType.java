package com.karadise.entities.database.adaptors;

import com.google.auto.service.AutoService;
import com.google.inject.Singleton;
import org.apache.cayenne.access.types.ExtendedType;
import org.bukkit.Material;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Singleton
@AutoService(ExtendedType.class)
public class MaterialType implements ExtendedType<Material> {
    @Override
    public String getClassName() {
        return Material.class.getName();
    }

    @Override
    public void setJdbcObject(PreparedStatement statement, Material material, int pos, int type, int scale) throws Exception {
        if (material == null) {
            statement.setNull(pos, type);
        } else {
            statement.setString(pos, material.name());
        }
    }

    @Override
    public Material materializeObject(ResultSet rs, int index, int type) throws Exception {
        return Material.valueOf(rs.getString(index));
    }

    @Override
    public Material materializeObject(CallableStatement rs, int index, int type) throws Exception {
        return Material.valueOf(rs.getString(index));
    }

    @Override
    public String toString(Material material) {
        return material == null ? "NULL" : material.name();
    }
}
