package com.karadise.entities.database.adaptors;

import com.google.auto.service.AutoService;
import com.google.inject.Singleton;
import com.rhonim.toolbox.entities.position.Position;
import org.apache.cayenne.access.types.ExtendedType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Singleton
@AutoService(ExtendedType.class)
public class PositionType implements ExtendedType<Position> {
    @Override
    public String getClassName() {
        return Position.class.getName();
    }

    @Override
    public void setJdbcObject(PreparedStatement statement, Position position, int pos, int type, int scale) throws Exception {
        if (position == null) {
            statement.setNull(pos, type);
        } else {
            statement.setString(pos, position.toString());
        }
    }

    @Override
    public Position materializeObject(ResultSet rs, int index, int type) throws Exception {
        return new Position(rs.getString(index));
    }

    @Override
    public Position materializeObject(CallableStatement rs, int index, int type) throws Exception {
        return new Position(rs.getString(index));
    }

    @Override
    public String toString(Position position) {
        return position == null ? "NULL" : position.toString();
    }
}
