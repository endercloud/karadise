package com.karadise.entities.gui;

import com.karadise.entities.database.generated.PlayerData;
import com.rhonim.toolbox.entities.gui.ActionContext;
import com.rhonim.toolbox.entities.gui.ActionableElement;
import com.rhonim.toolbox.entities.gui.GUI;
import com.rhonim.toolbox.entities.gui.Page;
import com.rhonim.toolbox.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PlayerSelectorGUI extends GUI {

    public PlayerSelectorGUI() {
    }

    public void open(Player player, String title, Map<Long, PlayerData> players, Consumer<Long> consumer) {
        this.open(player, title, players, consumer, 0);
    }

    public void open(Player player, String title, Map<Long, PlayerData> players, Consumer<Long> consumer, int page) {
        if (this.hasState("page-" + page)) {
            this.openState(player, "page-" + page);
        }
        Page.PageBuilder builder = Page.builder()
                .setParent(this)
                .setTitle(title)
                .setRows(6);

        if (page > 0) {
            builder.putElement(0, new ActionableElement(new ItemBuilder(Material.ARROW)
                    .withCustomName("" + ChatColor.WHITE + "< " + ChatColor.GOLD + "Back")
                    .build(),
                    new ActionContext((event, context) -> context.getGui().openState(player, "page-" + (page - 1)))));
        }

        if (players.size() - ((page + 1) * 45) > 0) {
            builder.putElement(9, new ActionableElement(new ItemBuilder(Material.ARROW)
                    .withCustomName("" + ChatColor.GOLD + "Next" + ChatColor.WHITE + " >")
                    .build(),
                    new ActionContext((event, context) -> context.getGui().openState(player, "page-" + (page + 1)))));
        }

        int index = 9;
        for (Map.Entry<Long, PlayerData> entry : players.entrySet().stream().limit(45).skip(page * 45).collect(Collectors.toList())) {
            builder.putElement(index++, new ActionableElement(new ItemBuilder(Material.PLAYER_HEAD)
                    .withCustomName("" + ChatColor.YELLOW + ChatColor.BOLD + entry.getValue().getUsername())
                    .withLore(
                            "",
                            "" + ChatColor.GOLD + ChatColor.BOLD + "Left-Click" + ChatColor.GRAY + " to " + title.toLowerCase()
                    )
                    .withSkinURL(entry.getValue().getSkin())
                    .build(),
                    new ActionContext((event, context) -> {
                            consumer.accept(entry.getKey());
                        ((Page) context.getState()).close(true);
                    })));
        }

        this.addState(builder.build("page-" + page));
        this.openState(player, "page-" + page);
    }

}
