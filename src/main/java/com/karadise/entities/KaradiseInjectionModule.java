package com.karadise.entities;

import com.google.inject.AbstractModule;
import com.karadise.Karadise;
import com.rhonim.toolbox.database.mysql.PluginCayanneClassLoaderProvider;
import org.bukkit.Server;

public class KaradiseInjectionModule extends AbstractModule {

    private final Karadise plugin;

    public KaradiseInjectionModule(Karadise plugin) {
        this.plugin = plugin;
    }

    @Override
    protected void configure() {
        bind(Karadise.class).toInstance(this.plugin);
        bind(Server.class).toInstance(this.plugin.getServer());
        bind(PluginCayanneClassLoaderProvider.class).toInstance(this.plugin);
    }
}
