package com.karadise;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.karadise.entities.KaradiseInjectionModule;
import com.karadise.tasks.CurrentClaimUpdater;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.database.mysql.MySQL;
import com.rhonim.toolbox.database.mysql.PluginCayanneClassLoaderProvider;
import com.rhonim.toolbox.entities.customitems.CustomItem;
import com.rhonim.toolbox.injection.AppInjector;
import com.rhonim.toolbox.injection.GuiceServiceLoader;
import com.rhonim.toolbox.injection.InjectionRoot;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.repo.CustomItemRepository;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import org.apache.cayenne.access.DataNode;
import org.apache.cayenne.access.types.ExtendedType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public final class Karadise extends JavaPlugin implements InjectionRoot, PluginCayanneClassLoaderProvider {

    @Inject
    private Injector injector;

    @Inject
    private LuckPerms luckPerms;

    @Override
    public void onLoad() {
        AppInjector.registerInjectionRoot(this);
        AppInjector.registerRootModule(new KaradiseInjectionModule(this));
    }

    @Override
    public void onEnable() {
        //Remove logs older than 30 days
        File logsFolder = new File("./logs/");
        File[] listOfLogs = logsFolder.listFiles();
        if (listOfLogs != null) {
            for (File log : listOfLogs) {
                try {
                    BasicFileAttributes attr = Files.getFileAttributeView(Paths.get(log.getCanonicalPath()), BasicFileAttributeView.class)
                            .readAttributes();
                    if (System.currentTimeMillis() - attr.creationTime().to(TimeUnit.MILLISECONDS) > TimeUnit.DAYS.toMillis(30)) {// 30 days
                        if (log.delete()) {
                            getLogger().info("Log " + log.getName() + " has been deleted.");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //Register DB adapters
        MySQL sql = this.injector.getInstance(MySQL.class);
        if (sql == null) {
            this.getServer().setWhitelist(true);
            getLogger().severe("SQL was null! Are we missing files?");
            getLogger().severe("Whitelist turned on!");
            return;
        }

        for (DataNode node : sql.getRuntime().getDataDomain().getDataNodes()) {
            if (node == null) {
                return;
            }
            this.getLogger().info("Registering extended types for " + node.getName() + "...");
            GuiceServiceLoader.load(ExtendedType.class, getClassLoader()).forEach(node.getAdapter().getExtendedTypes()::registerType);
        }

        //Register Services
        this.getLogger().info("Initiating Services...");
        GuiceServiceLoader.load(Service.class, getClassLoader()).forEach(Service::init);

        this.getLogger().info("Registering Events...");
        GuiceServiceLoader.load(Listener.class, getClassLoader()).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));

        this.getLogger().info("Registering Commands...");
        Bukkit.getCommandMap().getKnownCommands().remove("remove");
        GuiceServiceLoader.load(ToolboxCommand.class, getClassLoader()).forEach(command -> Bukkit.getCommandMap().register("karadise", command));

        this.getLogger().info("Registering Custom Items...");
        CustomItemRepository customItemRepo = injector.getInstance(CustomItemRepository.class);
        GuiceServiceLoader.load(CustomItem.class, getClassLoader()).forEach(customItemRepo::registerCustomItem);

        for (World world : Bukkit.getWorlds()) {
            WorldBorder worldBorder = world.getWorldBorder();
            worldBorder.setSize(10000d / (world.getEnvironment() != World.Environment.NETHER ? 1d : 8d)); //Fix nether size
            worldBorder.setCenter(0, 0);
//            world.setSpawnLocation(0, 100, 0); //Spawns have now been set. Remove in case we need to fix them
        }


        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        for (Team team : new ArrayList<>(scoreboard.getTeams())) {
            team.unregister();
        }

        for (Group g : this.luckPerms.getGroupManager().getLoadedGroups()) {
            String prefix = "";
            if (g.getCachedData().getMetaData().getPrefix() != null) {
                prefix = ChatColor.translateAlternateColorCodes('&', g.getCachedData().getMetaData().getPrefix());
            }

            String suffix = "";
            if (g.getCachedData().getMetaData().getSuffix() != null) {
                suffix = ChatColor.translateAlternateColorCodes('&', g.getCachedData().getMetaData().getSuffix());
            }


            Team team = scoreboard.registerNewTeam(g.getName().toLowerCase());
            if (!prefix.isEmpty()) {
                team.setPrefix(prefix + " ");
            }

            if (!suffix.isEmpty()) {
                team.setSuffix(suffix + " ");
            }
        }

        this.injector.getInstance(CurrentClaimUpdater.class).runTaskTimer(this, 0, 2);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
