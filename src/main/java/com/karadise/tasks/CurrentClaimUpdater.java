package com.karadise.tasks;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.karadise.entities.database.generated.Claim;
import com.karadise.entities.database.generated.Karadiser;
import com.karadise.repos.ClaimRepository;
import com.karadise.repos.KaradiserRepository;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Singleton
public class CurrentClaimUpdater extends BukkitRunnable {

    @Inject
    private KaradiserRepository karadiserRepository;
    @Inject
    private ClaimRepository claimRepository;

    private final List<UUID> processing = new ArrayList<>();

    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player == null) {
                continue;
            }
            if (processing.contains(player.getUniqueId())) {
                continue;
            }
            processing.add(player.getUniqueId());
            try {
                if (!player.isOnline())
                    continue;
                Karadiser data = this.karadiserRepository.getCachedKaradiser(player.getUniqueId());
                if (data == null) {
                    processing.remove(player.getUniqueId());
                    continue;
                }
                Claim claimStandingIn = this.claimRepository.getCachedClaim(player.getLocation());

                if (data.getCurrentClaim() != claimStandingIn) {
                    Claim oldClaimStandingIn = data.getCurrentClaim();
                    data.setCurrentClaim(claimStandingIn);
                    if (claimStandingIn == null) {
                        player.sendMessage(ChatColor.GRAY + "** WILDERNESS **");
                    } else {
                        if (oldClaimStandingIn == null || !oldClaimStandingIn.getOwner().equals(claimStandingIn.getOwner())) {
                            player.sendMessage(ChatColor.GRAY + "** " + claimStandingIn.getCachedOwnerName() + "'" +
                                    (claimStandingIn.getCachedOwnerName().toLowerCase().endsWith("s")?"":"s") + ChatColor.GRAY + " Claimed Land **");
                        }
                    }
                    data.save();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            processing.remove(player.getUniqueId());
        }
    }
}
