package com.karadise.tasks;

import com.destroystokyo.paper.MaterialSetTag;
import com.destroystokyo.paper.MaterialTags;
import com.karadise.Karadise;
import com.rhonim.toolbox.util.Scheduler;
import org.bukkit.*;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jackson
 * @version 1.0
 */
public class ChunkOutlineTask {

//    private static final List<Material> skipMaterials = Arrays.asList(
//            Material.AIR, Material.BLACK_BED, Material.BLUE_BED, Material.BROWN_BED, Material.CYAN_BED, Material.GRAY_BED, Material.GREEN_BED,
//            Material.LIGHT_BLUE_BED, Material.LIGHT_GRAY_BED, Material.LIME_BED, Material.MAGENTA_BED, Material.ORANGE_BED, Material.PINK_BED,
//            Material.PURPLE_BED, Material.RED_BED, Material.WHITE_BED, Material.YELLOW_BED, Material.BEDROCK, Material.END_PORTAL,
//            Material.NETHER_PORTAL, Material.GRASS, Material.TALL_GRASS, Material.SUNFLOWER, Material.ROSE_BUSH, Material.FLOWER_POT,
//            Material.BARRIER, Material.ACACIA_LEAVES, Material.BIRCH_LEAVES, Material.DARK_OAK_LEAVES, Material.JUNGLE_LEAVES,
//            Material.OAK_LEAVES, Material.SPRUCE_LEAVES, Material.CHEST, Material.TRAPPED_CHEST, Material.ENDER_CHEST,
//            Material.HOPPER
//    );

    @SuppressWarnings("unchecked")
    private static final MaterialSetTag SKIP_MATERIALS = new MaterialSetTag(new NamespacedKey(JavaPlugin.getPlugin(Karadise.class), "chunk_outline_exclude"),
            Material.AIR, Material.GRASS, Material.TALL_GRASS, Material.SUNFLOWER, Material.ROSE_BUSH, Material.FLOWER_POT, Material.BARRIER,
            Material.CHEST, Material.TRAPPED_CHEST, Material.ENDER_CHEST, Material.HOPPER)
            .add(Tag.BEDS, Tag.PORTALS, Tag.LEAVES, Tag.SAPLINGS, Tag.BUTTONS, Tag.RAILS)
            .add(MaterialTags.TORCHES);

    private static final BlockData GLOW = Bukkit.getServer().createBlockData(Material.GLOWSTONE);

    private final Player p;
    private final ChunkSnapshot chunk;

    private final HashMap<Location, BlockData> restoreData = new HashMap<>();

    public ChunkOutlineTask(Player p, ChunkSnapshot chunk) {
        this.p = p;
        this.chunk = chunk;
    }

    public void run() {
        Scheduler.async(() -> {
            for (int x = 0; x < 16; x++) {
                for (int z = 0; z < 16; z++) {
                    if (x == 0 || x == 15 || z == 0 || z == 15) {
                        for (int y = 0; y < 256; y++) {
                            BlockData block = chunk.getBlockData(x, y, z);
                            if (block.getMaterial() == Material.WATER
                                    || block.getMaterial() == Material.LAVA) {
                                continue;
                            }
                            if (SKIP_MATERIALS.isTagged(block)) {
                                continue;
                            }
                            /*boolean hasAirBlock = false;
                            for (BlockFace face : BlockFace.values()) {
                                if (!face.equals(BlockFace.DOWN)
                                        && !face.equals(BlockFace.UP)
                                        && !face.equals(BlockFace.NORTH)
                                        && !face.equals(BlockFace.EAST)
                                        && !face.equals(BlockFace.SOUTH)
                                        && !face.equals(BlockFace.WEST)) {
                                    continue;
                                }

                                if (SKIP_MATERIALS.isTagged(chunk.getBlockData(x + face.getModX(), y + face.getModY(), z + face.getModZ()))) {
                                    hasAirBlock = true;
                                }
                                if ((block.getMaterial() != Material.WATER) && (block.get(face).getType() == Material.WATER)) {
                                    hasAirBlock = true;
                                }
                            }
                            if (!hasAirBlock)
                                continue;*/
                            Location loc = new Location(p.getWorld(), (chunk.getX() << 4) | x, y, (this.chunk.getZ() << 4) | z);
                            restoreData.put(loc, block);
                            p.sendBlockChange(loc, GLOW);
                        }
                    }
                }
            }
        });
        Scheduler.laterAsync(() -> {
            for (Map.Entry<Location, BlockData> entry : restoreData.entrySet()) {
                p.sendBlockChange(entry.getKey(), entry.getValue());
            }
        }, 20 * 10);
    }
}
